﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ecomm.util.entities
{
    public class Disc
    {
        public string type { get; set; }
        public string key { get; set; }
        public string key_text { get; set; }
        public string val { get; set; }

    }
}
