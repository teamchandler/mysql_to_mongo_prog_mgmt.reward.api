﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ecomm.util.entities
{
    public class cart
    {
        public string cart_id { get; set; }
        public Int64 company_id { get; set; }
        public string user_id { get; set; }
        public string id { get; set; }
        public decimal quantity { get; set; }
        public decimal unit_price { get; set; }
        public decimal mrp { get; set; }
        public decimal final_offer { get; set; }
        public DateTime create_ts { get; set; }
        public DateTime modify_ts { get; set; }
        public int status { get; set; }

    }

    public class shipping
    {
        public string cart_id { get; set; }
        public Int64 order_id { get; set; }
        public string user_id { get; set; }
        public string name { get; set; }
        public Int64 pincode { get; set; }
        public string address { get; set; }
        public string state { get; set; }
        public string city { get; set; }
        public string country { get; set; }
        public string landmark { get; set; }
        public string mobile_number { get; set; }
    }
    public class order
    {
        public Int64 order_id { get; set; }
        public string cart_id { get; set; }
        public string user_id { get; set; }
        public string discount_code { get; set; }
        public decimal total_amount { get; set; }
        public string total_amount_final_offer { get; set; }
        public Int64 points { get; set; }
        public decimal paid_amount { get; set; }
        public int status { get; set; }
        public string payment_info { get; set; }
        public string rejection_reason { get; set; }
        public DateTime create_ts { get; set; }
        public DateTime modify_ts { get; set; }
        public DateTime pay_sent_ts { get; set; }
        public DateTime pay_conf_ts { get; set; }
    }

    public class ord
    {
        public Int64 order_id { get; set; }
        public string cart_id { get; set; }
        public string user_id { get; set; }
        public string discount_code { get; set; }
        public decimal total_amount { get; set; }
        public string total_amount_final_offer { get; set; }
        public Int64 points { get; set; }
        public decimal paid_amount { get; set; }
        public int status { get; set; }
        public string payment_info { get; set; }
        public string rejection_reason { get; set; }
        public DateTime create_ts { get; set; }
        public DateTime modify_ts { get; set; }
        public DateTime pay_sent_ts { get; set; }
        public DateTime pay_conf_ts { get; set; }
        public string order_display_id { get; set; }
        public string store { get; set; }
        public string Order_Status { get; set; }
        public string BillToName { get; set; }
        public string ShipToName { get; set; }
        public string order_date { get; set; }
    }

    public class ordadmin
    {
        public Int64 order_id { get; set; }
        public string cart_id { get; set; }
        public string user_id { get; set; }
        public string discount_code { get; set; }
        public decimal total_amount { get; set; }
        public string total_amount_final_offer { get; set; }
        public Int64 points { get; set; }
        public double paid_amount { get; set; }
        public int status { get; set; }
        public string payment_info { get; set; }
        public string rejection_reason { get; set; }
        public DateTime create_ts { get; set; }
        public DateTime modify_ts { get; set; }
        public DateTime pay_sent_ts { get; set; }
        public DateTime pay_conf_ts { get; set; }
        public string order_display_id { get; set; }
        public string store { get; set; }
        public string Order_Status { get; set; }
        public string BillToName { get; set; }
        public string ShipToName { get; set; }
        public string order_date { get; set; }
        public string billing_address { get; set; }
        public string Cust_Name { get; set; }
        public string email_id { get; set; }
        public string contact_number { get; set; }
        public string bal_points { get; set; }
        public double points_amount { get; set; }
        public string egift_vou_no { get; set; }
        public double egift_vou_amt { get; set; }
        public List<cart_item> cart_data { get; set; }
        public string shipping_info { get; set; }
        public string delayed_note { get; set; }
        public string cancellation_note { get; set; }

        public string courier_track_no { get; set; }
        public string courier_track_link { get; set; }

        public double courier_cost { get; set; }
        public string shipping_date { get; set; }

        public Int64 Delay_Days { get; set; }

        public dispute dispute_data { get; set; }
        //public List<dispute> dispute_notes { get; set; }

        public int send_email { get; set; }
    }


    public class user_points
    {
        public string user_id { get; set; }
        public decimal txn_amount { get; set; }
        public string txn_type { get; set; } //DR,CR
        public DateTime txn_ts { get; set; }
        public string txn_comment { get; set; }
        public int txn_status { get; set; }

    }

    public class paymentinfostatus
    {
        public Int64 order_id { get; set; }
        public int status { get; set; }
    }

    public class payment_points_gifts
    {
        public string EmailID { get; set; }
        public string OrderID { get; set; }
        public string PaidAmt { get; set; }
        public string Points { get; set; }
        public string Points_INR { get; set; }
        public string strActualAmt { get; set; }
        public string egiftVouNo { get; set; }
        public string egiftVouAmt { get; set; }
        public string discount_coupon { get; set; }
        public string discount_amount { get; set; }
        public string status { get; set; }
    }
}
