﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ecomm.util.entities
{
    public class OutCollection
    {
        public string strParamName { get; set; }
        public string strParamValue { get; set; }
    }
}
