﻿﻿﻿﻿using ecomm.dal;
using ecomm.util.entities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using Newtonsoft.Json;
using System.Security.Cryptography;
using ecomm.util;


namespace ecomm.model.repository
{
    public class gift_repository
    {

        private bool check_field(BsonDocument doc, string field_name)
        {
            if (doc.Contains(field_name))
            {
                return true;
            }
            else
            {
                return false;
            }
        }


        private int convertToInt(object o)
        {
            try
            {
                if (o != null)
                {
                    return Int32.Parse(o.ToString());
                }
                else { return 0; }
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return 0;
            }
        }
        private decimal convertToDecimal(object o)
        {
            try
            {
                if (o != null)
                {
                    return decimal.Parse(o.ToString());
                }
                else { return 0; }
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return 0;
            }
        }
        private long convertToLong(object o)
        {
            try
            {
                if (o != null)
                {
                    return long.Parse(o.ToString());
                }
                else { return 0; }
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return 0;
            }
        }
        private float convertToFloat(object o)
        {
            try
            {
                if (o != null)
                {
                    return float.Parse(Math.Round(decimal.Parse(o.ToString()), 3).ToString());
                }
                else { return 0; }
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return 0;
            }
        }
        private double convertToDouble(object o)
        {
            try
            {
                if (o != null)
                {
                    return double.Parse(Math.Round(decimal.Parse(o.ToString()), 3).ToString());
                }
                else { return 0; }
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return 0;
            }
        }
        private string convertToString(object o)
        {
            try
            {
                if ((o != null) && (o.ToString() != ""))
                {
                    return o.ToString();
                }
                else { return ""; }
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return "";
            }
        }
        private DateTime convertToDate(object o)
        {
            try
            {
                if ((o != null) && (o != ""))
                {
                    //string f = "mm/dd/yyyy";
                    return DateTime.Parse(o.ToString());
                    //return DateTime.ParseExact(o.ToString(), "MM-dd-yy", System.Globalization.CultureInfo.InvariantCulture);
                }
                else { return DateTime.Parse("1/1/1800"); }
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return DateTime.Parse("1/1/1800");
            }
        }

        private QueryDocument createQueryDocument(string s)
        {
            BsonDocument document = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>(s);
            QueryDocument queryDoc = new QueryDocument(document);
            return queryDoc;
        }
        private UpdateDocument createUpdateDocument(string s)
        {
            BsonDocument document = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>(s);
            UpdateDocument updateDoc = new UpdateDocument(document);
            return updateDoc;
        }
        private string GetUniqueKey()
        {
            try
            {
                int maxSize = 7;
                char[] chars = new char[62];
                string a;
                a = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
                chars = a.ToCharArray();
                RNGCryptoServiceProvider crypto = new RNGCryptoServiceProvider();
                int size = maxSize;
                byte[] data = new byte[size];
                crypto.GetNonZeroBytes(data);
                //int value = BitConverter.ToInt32(data, 0);
                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < data.Length; i++)
                {
                    sb.Append(data[i].ToString("X2"));
                }
                return sb.ToString();
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return "";
            }

        }
        private string[] mongo_query(string qs, string collection, string[] include_fields, string[] exclude_fields)
        {
            List<String> list = new List<string>();
            try
            {
                dal.DataAccess dal = new DataAccess();
                string q = qs;//"{cat_id:{id:'" + cat_id + "'}}";
                BsonDocument document = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>(q);
                QueryDocument queryDoc = new QueryDocument(document);
                MongoCursor cursor = dal.execute_mongo_db(collection, queryDoc, include_fields, exclude_fields);
                
                foreach (var c in cursor)
                {

                    list.Add(c.ToBsonDocument().ToString());
                }
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
            }
            return list.ToArray();
        }
        private string mongo_query_singleton(string qs, string collection, string[] include_fields, string[] exclude_fields)
        {
            try
            {
                dal.DataAccess dal = new DataAccess();
                string q = qs;//"{cat_id:{id:'" + cat_id + "'}}";
                BsonDocument document = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>(q);
                QueryDocument queryDoc = new QueryDocument(document);
                MongoCursor cursor = dal.execute_mongo_db(collection, queryDoc, include_fields, exclude_fields);
                List<String> list = new List<string>();
                if (cursor.Size() > 0)
                {
                    foreach (var c in cursor)
                    {
                        list.Add(c.ToBsonDocument().ToString());
                    }
                    return list[0];
                }
                else
                {
                    return "-1";
                }
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return "-1";
            }
        }
        private MongoCursor mongo_query_singleton_cursor(string qs, string collection, string[] include_fields, string[] exclude_fields)
        {
            dal.DataAccess dal = new DataAccess();
            string q = qs;//"{cat_id:{id:'" + cat_id + "'}}";
            BsonDocument document = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>(q);
            QueryDocument queryDoc = new QueryDocument(document);
            MongoCursor cursor = dal.execute_mongo_db(collection, queryDoc, include_fields, exclude_fields);
            return cursor;
        }


        #region gift voucher mongo write

        public string insert_gv(gift gv)
        {
            try
            {
                Boolean bln = false;
                //evoke mongodb connection method in dal//
                string result = "";
                string toemail = "";
                string strFileName = "";
                string strLine = "";
                strFileName = System.Configuration.ConfigurationSettings.AppSettings["egiftcsv"].ToString() + "\\" + Guid.NewGuid() + ".csv";
                System.IO.StreamWriter sw = new System.IO.StreamWriter(strFileName);

                sw.WriteLine("VoucherCode,Amount");
                for (int i = 0; i < gv.NoOfVou; i++)
                {
                    bln = true;
                    toemail = gv.rcv_email;
                    gv._id = ObjectId.GenerateNewId().ToString();
                    gv.gift_voucher_code = GetUniqueKey();
                    gv.create_date = System.DateTime.Now.ToLocalTime();
                    gv.active = 1;// 0=booked, 1=active, 2 = redemmed
                    gv.status = "booked"; // Booked, paid, pay_rejected, sent_to_customer, redeemed 
                    dal.DataAccess dal = new DataAccess();
                    BsonDocument bd_gift = gv.ToBsonDocument();
                    result = dal.mongo_write("gift_cert", bd_gift);
                    strLine = gv.gift_voucher_code + "," + gv.amount.ToString();
                    sw.WriteLine(strLine);
                }
                sw.Close();

                if (bln == true)
                {
                    helper_repository hr = new helper_repository();
                    registration_repository rr = new registration_repository();
                    string bcclist = System.Configuration.ConfigurationSettings.AppSettings["MailGiftTo"].ToString();
                    //string bcclist = System.Configuration.ConfigurationSettings.AppSettings["Management"].ToString();
                    string strBodyEmail = "Hi There";
                    strBodyEmail += "<br/>";
                    strBodyEmail += "<br/>";
                    strBodyEmail += "Please find the newly generated e-Gift Voucher Numbers as attachment.";
                    strBodyEmail += "<br/>";
                    strBodyEmail += "<br/>";
                    strBodyEmail += "Regards,";
                    strBodyEmail += "<br/>";
                    strBodyEmail += "<br/>";
                    strBodyEmail += "Annectos Support Team";
                    hr.SendMailWithAttachment(rr.getUserEMailAddress(toemail), "", bcclist, "E-Gift Voucher Generated", strBodyEmail, strFileName);
                }

                return result;
            }
            catch (Exception ex)
            {
                return "";
            }
        }

        public string update_gv_status(string gv_code, string status)
        {
            try
            {
                //evoke mongodb connection method in dal
                // create query
                string q = "{ gift_voucher_code : '" + gv_code + "'}";
                QueryDocument queryDoc = createQueryDocument(q);
                // create update 
                //string s = "{ status : '" + status + "'}";
                //UpdateDocument updateDoc = createUpdateDocument(s);
                UpdateBuilder u = new UpdateBuilder();
                u.Set("status", status);

                // exec
                dal.DataAccess dal = new DataAccess();
                BsonDocument result = dal.mongo_find_and_modify("gift_cert", queryDoc, u);

                return result.ToString();
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return "";
            }
        }
        public string post_redeem_voucher_code(gift gf)
        {
            try
            {
                //evoke mongodb connection method in dal
                // create query
                string q = "{ gift_voucher_code : '" + gf.gift_voucher_code + "'}";
                QueryDocument queryDoc = createQueryDocument(q);
                // create update 
                UpdateBuilder u = new UpdateBuilder();
                if (gf.validity == 0)
                {
                    u.Set("status", gf.status).Set("active", gf.active);
                }
                else
                {
                    u.Set("validity", gf.validity);
                }
                // exec
                dal.DataAccess dal = new DataAccess();
                BsonDocument result = dal.mongo_find_and_modify("gift_cert", queryDoc, u);
                return result.ToString();
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return "";
            }
        }


        public string redeem_gv(string gv_code)
        {
            //evoke mongodb connection method in dal
            // create query

            try
            {
                string q = "{ gift_voucher_code : '" + gv_code + "'}";
                QueryDocument queryDoc = createQueryDocument(q);
                // create update 
                //string s = "{ status : '" + status + "'}";
                //UpdateDocument updateDoc = createUpdateDocument(s);
                UpdateBuilder u = new UpdateBuilder();
                u.Set("status", "redeemed")
                    .Set("active", 2);

                // exec
                dal.DataAccess dal = new DataAccess();
                BsonDocument result = dal.mongo_find_and_modify("gift_cert", queryDoc, u);

                return result.ToString();
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return "";
            }
        }
        // This uses cryptography to generate Random Number 
        // but this won't confirm zero collission as it's <32 byte. So 
        // we define gift_voucher_code in Mongo DB as unique also to stop dup

        #endregion


        #region gift voucher mongo read
        public gift validate_gv(string gv_code, string store)
        {
            gift g = new gift();
            try
            {
                // evoke mongodb connection method in dal
                dal.DataAccess dal = new DataAccess();
                string q = "{'gift_voucher_code':'" + gv_code + "', 'company':'" + store.ToLower() + "', 'active' : 1}}";
                BsonDocument document = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>(q);
                QueryDocument queryDoc = new QueryDocument(document);
                string[] include_fields = new string[] { "amount", "giver_email", "rcv_email" };
                string[] exclude_fields = new string[] { "_id" };
                //string[] ascending_sort_fields = {"price.list", "price.discount"};
                MongoCursor cursor = mongo_query_singleton_cursor(q, "gift_cert", include_fields, exclude_fields);
                
                if (cursor.Size() > 0)
                {
                    foreach (var c in cursor)
                    {
                        product prod = new product();
                        //prod.Id = c.ToBsonDocument()["_id"].ToString();
                        //prod.prod_name = c.ToBsonDocument()["prod_name"].ToString();
                        //g.gift_voucher_code = gv_code;
                        g.amount = convertToInt(c.ToBsonDocument()["amount"].ToString());
                        g.giver_email = c.ToBsonDocument()["giver_email"].ToString();
                        g.rcv_email = c.ToBsonDocument()["rcv_email"].ToString();

                    }
                }
                return g;
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return g;
            }
        }
        public gift get_gv_details(string gv_code)
        {
            gift g = new gift();
            try
            {
                // evoke mongodb connection method in dal
                dal.DataAccess dal = new DataAccess();
                string q = "{'gift_voucher_code':'" + gv_code + "'}}";
                BsonDocument document = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>(q);
                QueryDocument queryDoc = new QueryDocument(document);
                string[] include_fields = new string[] { "amount", "giver_email", "rcv_email", "validity", "create_date" };
                string[] exclude_fields = new string[] { "_id" };
                //string[] ascending_sort_fields = {"price.list", "price.discount"};
                MongoCursor cursor = mongo_query_singleton_cursor(q, "gift_cert", include_fields, exclude_fields);

                if (cursor.Size() > 0)
                {
                    foreach (var c in cursor)
                    {
                        product prod = new product();
                        //prod.Id = c.ToBsonDocument()["_id"].ToString();
                        //prod.prod_name = c.ToBsonDocument()["prod_name"].ToString();
                        //g.gift_voucher_code = gv_code;
                        g.amount = convertToInt(c.ToBsonDocument()["amount"].ToString());
                        g.giver_email = c.ToBsonDocument()["giver_email"].ToString();
                        g.rcv_email = c.ToBsonDocument()["rcv_email"].ToString();
                        g.validity = convertToInt(c.ToBsonDocument()["rcv_email"].ToString());
                        g.create_date = convertToDate(c.ToBsonDocument()["create_date"].ToString());

                    }
                }
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
            }
            return g;
        }

        public List<gift> get_all_voucher_code(string gv_status)
        {

                var gifts = new List<gift>();
            // evoke mongodb connection method in dal
            try
            {
                dal.DataAccess dal = new DataAccess();
                string q = "{'active' : 1}}";
                BsonDocument document = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>(q);
                QueryDocument queryDoc = new QueryDocument(document);
                //string[] include_fields = new string[] { "amount", "giver_email", "rcv_email" };
                //string[] exclude_fields = new string[] { "_id" };
                ////string[] ascending_sort_fields = {"price.list", "price.discount"};
                //MongoCursor cursor = mongo_query_singleton_cursor(q, "gift_cert", include_fields, exclude_fields);
                //BsonDocument document = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>("{active:'" + gv_status + "'}");
                //QueryDocument queryDoc = new QueryDocument(document);
                MongoCursor cursor = dal.execute_mongo_db("gift_cert", queryDoc);


                //MongoCursor cursor = dal.execute_mongo_db("gift_cert");


                foreach (var c in cursor)
                {
                    gift g = new gift();
                    g.gift_voucher_code = check_field(c.ToBsonDocument(), "gift_voucher_code") ? convertToString(c.ToBsonDocument()["gift_voucher_code"]) : null;
                    g.amount = check_field(c.ToBsonDocument(), "amount") ? convertToInt(c.ToBsonDocument()["amount"]) : 0;
                    g.company = check_field(c.ToBsonDocument(), "company") ? convertToString(c.ToBsonDocument()["company"]) : null;
                    g.validity = check_field(c.ToBsonDocument(), "validity") ? convertToInt(c.ToBsonDocument()["validity"]) : 0;
                    DateTime VouValidity = check_field(c.ToBsonDocument(), "create_date") ? convertToDate(c.ToBsonDocument()["create_date"]) : DateTime.Parse("1/1/1800");
                    VouValidity = VouValidity.AddDays(g.validity);
                    g.create_date = VouValidity;
                    gifts.Add(g);
                }
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
            }
            return gifts;
        }

        public List<gift> get_GV_Voucher_Details(string store, string fromdt, string todt)
        {
            var gifts = new List<gift>();
            // evoke mongodb connection method in dal
            try
            {
                dal.DataAccess dal = new DataAccess();
                string q = "{'company':'" + store + "', 'active' : 1}";
                BsonDocument document = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>(q);
                QueryDocument queryDoc = new QueryDocument(document);

                MongoCursor cursor = dal.execute_mongo_db("gift_cert", queryDoc);

                
                foreach (var c in cursor)
                {
                    gift g = new gift();
                    g.gift_voucher_code = check_field(c.ToBsonDocument(), "gift_voucher_code") ? convertToString(c.ToBsonDocument()["gift_voucher_code"]) : null;
                    g.amount = check_field(c.ToBsonDocument(), "amount") ? convertToInt(c.ToBsonDocument()["amount"]) : 0;
                    g.company = check_field(c.ToBsonDocument(), "company") ? convertToString(c.ToBsonDocument()["company"]) : null;
                    g.rcv_email = check_field(c.ToBsonDocument(), "rcv_email") ? convertToString(c.ToBsonDocument()["rcv_email"]) : null;
                    g.giver_email = check_field(c.ToBsonDocument(), "giver_email") ? convertToString(c.ToBsonDocument()["giver_email"]) : null;
                    g.validity = check_field(c.ToBsonDocument(), "validity") ? convertToInt(c.ToBsonDocument()["validity"]) : 0;
                    g.create_date = check_field(c.ToBsonDocument(), "create_date") ? convertToDate(c.ToBsonDocument()["create_date"]) : DateTime.Parse("1/1/1800");
                    //DateTime VouValidity = check_field(c.ToBsonDocument(), "create_date") ? convertToDate(c.ToBsonDocument()["create_date"]) : DateTime.Parse("1/1/1800");
                    //VouValidity = VouValidity.AddDays(g.validity);
                    //g.create_date = VouValidity;
                    gifts.Add(g);
                }
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
            }
            return gifts;
        }


        public gv_voucher get_find_voucher_code(string gv_code, string store)
        {
            // evoke mongodb connection method in dal
            gv_voucher g = new gv_voucher();
            try
            {
                dal.DataAccess dal = new DataAccess();
                //var ValGift = new gift();
                //ValGift= validate_gv(gv_code, store);

                string q = "{'gift_voucher_code':'" + gv_code + "', 'company':'" + store + "'}";
                BsonDocument document = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>(q);
                QueryDocument queryDoc = new QueryDocument(document);
                MongoCursor cursor = dal.execute_mongo_db("gift_cert", queryDoc);

                
                if (cursor.Size() > 0)
                {
                    foreach (var c in cursor)
                    {
                        g.gift_voucher_code = check_field(c.ToBsonDocument(), "gift_voucher_code") ? convertToString(c.ToBsonDocument()["gift_voucher_code"]) : null;
                        g.amount = check_field(c.ToBsonDocument(), "amount") ? convertToInt(c.ToBsonDocument()["amount"]) : 0;
                        g.company = check_field(c.ToBsonDocument(), "company") ? convertToString(c.ToBsonDocument()["company"]) : null;
                        g.status = check_field(c.ToBsonDocument(), "status") ? convertToString(c.ToBsonDocument()["status"]) : null;
                        g.validity = check_field(c.ToBsonDocument(), "validity") ? convertToInt(c.ToBsonDocument()["validity"]) : 0;
                        g.create_date = check_field(c.ToBsonDocument(), "create_date") ? convertToDate(c.ToBsonDocument()["create_date"]) : DateTime.Parse("1/1/1800");
                        DateTime VouValidity = check_field(c.ToBsonDocument(), "create_date") ? convertToDate(c.ToBsonDocument()["create_date"]) : DateTime.Parse("1/1/1800");
                        VouValidity = VouValidity.AddDays(g.validity);
                        g.valid_date = VouValidity;

                    }
                }
                
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
            }
            return g;
        }


        public string get_gv_by_code(string gv_code, string status)
        {
            try
            {
                // evoke mongodb connection method in dal
                dal.DataAccess dal = new DataAccess();
                string q = "{'gift_voucher_code':'" + gv_code + "', 'status':'" + status + "'}}";
                BsonDocument document = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>(q);
                QueryDocument queryDoc = new QueryDocument(document);
                string[] include_fields = new string[] { "amount", "giver_email", "rcv_email" };
                string[] exclude_fields = new string[] { "_id" };
                //string[] ascending_sort_fields = {"price.list", "price.discount"};
                return mongo_query_singleton(q, "gift_cert", include_fields, exclude_fields);
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return "";
            }
        }
        public string get_gv_list_by_user(string user, string status)
        {
            try
            {
                // evoke mongodb connection method in dal
                dal.DataAccess dal = new DataAccess();
                string q = "{'rcv_email':'" + user + "', 'status':'" + status + "'}}";
                BsonDocument document = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>(q);
                QueryDocument queryDoc = new QueryDocument(document);
                string[] include_fields = new string[] { "amount", "giver_email", "rcv_email", "create_date", "validity" };
                string[] exclude_fields = new string[] { "_id" };
                //string[] ascending_sort_fields = {"price.list", "price.discount"};
                return mongo_query_singleton(q, "gift_cert", include_fields, exclude_fields);

            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return "";
            }
        }
        public string get_gv_all_by_user(string user)
        {
            // evoke mongodb connection method in dal
            try
            {
                dal.DataAccess dal = new DataAccess();
                string q = "{'rcv_email':'" + user + "'}}";
                BsonDocument document = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>(q);
                QueryDocument queryDoc = new QueryDocument(document);
                string[] include_fields = new string[] { "amount", "giver_email", "rcv_email", "create_date", "validity", "status" };
                string[] exclude_fields = new string[] { "_id" };
                //string[] ascending_sort_fields = {"price.list", "price.discount"};
                return mongo_query_singleton(q, "gift_cert", include_fields, exclude_fields);
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return "";
            }
        }

        public string postExportVouList(expo_vou_grid evg)
        {

            DataAccess da = new DataAccess();
            List<OutCollection> oc = new List<OutCollection>();

            try
            {
                Boolean bln = false;
                //evoke mongodb connection method in dal//
                string result = "";
                string toemail = "";
                string strFileName = "";
                string strLine = "";
                string strVouFileId = "";
                strVouFileId = Guid.NewGuid().ToString();
                strFileName = System.Configuration.ConfigurationSettings.AppSettings["ordercsv"].ToString() + "\\" + strVouFileId + ".csv";
                System.IO.StreamWriter sw = new System.IO.StreamWriter(strFileName);

                sw.WriteLine("GV Code,Created Date,From Email,To Email,Amount,Validity");
                for (int i = 0; i < evg.GridData.Count(); i++)
                {
                    bln = true;
                    strLine = convertToString(evg.GridData[i].gift_voucher_code) + "," + convertToString(evg.GridData[i].create_date) + "," + convertToString(evg.GridData[i].giver_email) + "," + convertToString(evg.GridData[i].rcv_email) + "," + convertToString(evg.GridData[i].amount) + "," + convertToString(evg.GridData[i].validity);
                    sw.WriteLine(strLine);
                }
                sw.Close();
                return strVouFileId;
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                throw ex;
            }



        }

        #endregion


        //#region ===Gift Voucher Insert===
        ///// <summary>
        ///// Insert Gift Voucher
        ///// </summary>
        ///// <param name="gft"></param>
        ///// <returns>OutCollection</returns>
        //public OutCollection GiftVoucherInsert(gift gft)
        //{
        //    DataAccess da = new DataAccess();
        //    List<OutCollection> oc = new List<OutCollection>();
        //    OutCollection output = new OutCollection();
        //    try
        //    {
        //        int i = da.ExecuteSP("user_gift_voucher_insert", ref oc
        //                        , da.Parameter("_gift_voucher_code", gft.gift_voucher_code)
        //                        , da.Parameter("_email_id", gft.email_id)
        //                        , da.Parameter("_status", gft.status)
        //                        , da.Parameter("_amount", gft.amount)
        //                        , da.Parameter("_write_by", gft.write_by)
        //                        , da.Parameter("_expiry_date", gft.expiry_date));

        //        if (i == 1)
        //        {
        //            output.strParamName = "Message";
        //            output.strParamValue = "Success";
        //            return output;
        //        }
        //        else
        //        {
        //            output.strParamName = "Message";
        //            output.strParamValue = "Failed";
        //            return output;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        output.strParamName = "Message";
        //        output.strParamValue = ex.Message;
        //        return output;
        //    }

        //} 
        //#endregion


        //#region ===Gift Voucher Update===
        ///// <summary>
        ///// Update Gift Voucher
        ///// </summary>
        ///// <param name="gft"></param>
        ///// <returns></returns>
        //public OutCollection GiftVoucherUpdate(gift gft)
        //{
        //    DataAccess da = new DataAccess();
        //    List<OutCollection> oc = new List<OutCollection>();
        //    OutCollection output = new OutCollection();
        //    try
        //    {
        //        int i = da.ExecuteSP("user_gift_voucher_update", ref oc
        //                        , da.Parameter("_gift_voucher_code", gft.gift_voucher_code)
        //                        , da.Parameter("_email_id", gft.email_id)
        //                        , da.Parameter("_status", gft.status)
        //                        , da.Parameter("_amount", gft.amount)
        //                        , da.Parameter("_write_by", gft.write_by)
        //                        , da.Parameter("_expiry_date", gft.expiry_date));

        //        if (i == 1)
        //        {
        //            output.strParamName = "Message";
        //            output.strParamValue = "Success";
        //            return output;
        //        }
        //        else
        //        {
        //            output.strParamName = "Message";
        //            output.strParamValue = "Failed";
        //            return output;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        output.strParamName = "Message";
        //        output.strParamValue = ex.Message;
        //        return output;
        //    }

        //} 
        //#endregion
        //#region ===Gift Voucher By User===
        ///// <summary>
        ///// Update Gift Voucher
        ///// </summary>
        ///// <param name="gft"></param>
        ///// <returns></returns>
        //public List<gift> get_voucher_by_user(string email_id)
        //{
        //    DataAccess da = new DataAccess();
        //    List<gift> giftlist = new List<gift>();
        //    try
        //    {
        //        DataTable dt = da.ExecuteDataTable("get_voucher_by_user" 
        //                        , da.Parameter("email_id", email_id));

        //        for (int i = 0; i < dt.Rows.Count; i++)
        //        {
        //            gift gft = new gift();
        //            gft._id = Convert.ToInt64(dt.Rows[i]["_id"].ToString());
        //            gft.amount = Convert.ToDecimal(dt.Rows[i]["amount"].ToString());
        //            gft.email_id = dt.Rows[i]["email_id"].ToString();
        //            gft.expiry_date = Convert.ToDateTime(dt.Rows[i]["expiry_date"]);
        //            gft.gift_voucher_code = dt.Rows[i]["gift_voucher_code"].ToString();
        //            gft.status = dt.Rows[i]["status"].ToString();
        //            gft.write_by = dt.Rows[i]["write_by"].ToString();
        //            gft.write_ts = Convert.ToDateTime(dt.Rows[i]["write_ts"].ToString());
        //            giftlist.Add(gft);
        //        }

        //    }
        //    catch (Exception ex)
        //    {
        //        return null;
        //    }
        //    return giftlist;
        //}
        //#endregion
        //#region ===Gift Voucher By User===
        ///// <summary>
        ///// Update Gift Voucher
        ///// </summary>
        ///// <param name="gft"></param>
        ///// <returns></returns>
        //public List<gift> get_voucher_by_voucher_code(string gift_voucher_code)
        //{
        //    DataAccess da = new DataAccess();
        //    List<gift> giftlist = new List<gift>();
        //    try
        //    {
        //        DataTable dt = da.ExecuteDataTable("get_voucher_details"
        //                        , da.Parameter("gift_voucher_code", gift_voucher_code));

        //        for (int i = 0; i < dt.Rows.Count; i++)
        //        {
        //            gift gft = new gift();
        //            gft._id = Convert.ToInt64(dt.Rows[i]["_id"].ToString());
        //            gft.amount = Convert.ToDecimal(dt.Rows[i]["amount"].ToString());
        //            gft.email_id = dt.Rows[i]["email_id"].ToString();
        //            gft.expiry_date = Convert.ToDateTime(dt.Rows[i]["expiry_date"]);
        //            gft.gift_voucher_code = dt.Rows[i]["gift_voucher_code"].ToString();
        //            gft.status = dt.Rows[i]["status"].ToString();
        //            gft.write_by = dt.Rows[i]["write_by"].ToString();
        //            gft.write_ts = Convert.ToDateTime(dt.Rows[i]["write_ts"].ToString());
        //            giftlist.Add(gft);
        //        }

        //    }
        //    catch (Exception ex)
        //    {
        //        return null;
        //    }
        //    return giftlist;
        //}
        //#endregion


    }
}
