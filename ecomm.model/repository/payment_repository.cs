﻿
using ecomm.dal;
using ecomm.util;
using ecomm.util.entities;
using MongoDB.Bson;
using MongoDB.Driver;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ecomm.model.repository
{
    public class payment_repository
    {
        string strMailSubCompany = "";

        private bool check_field(BsonDocument doc, string field_name)
        {
            if (doc.Contains(field_name))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private DateTime convertToDate(object o)
        {
            try
            {
                if ((o != null) && (o != ""))
                {
                    //string f = "mm/dd/yyyy";
                    return DateTime.Parse(o.ToString());
                    //return DateTime.ParseExact(o.ToString(), "MM-dd-yy", System.Globalization.CultureInfo.InvariantCulture);
                }
                else { return DateTime.Parse("1/1/1800"); }
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return DateTime.Parse("1/1/1800");
            }
        }

        private string convertToString(object o)
        {
            try
            {
                if ((o != null) && (o.ToString() != "") && (o.ToString() != "BsonNull"))
                {
                    return o.ToString();
                }
                else if (o.ToString() == "BsonNull")
                {
                    return "";
                }
                else { return ""; }
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return "";
            }
        }
        private int convertToInt(object o)
        {
            try
            {
                if (o != null)
                {
                    return Int32.Parse(o.ToString());
                }
                else { return 0; }
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return 0;
            }
        }
        private double convertToDouble(object o)
        {
            try
            {
                if (o != null && o.ToString() != "")
                {
                    return double.Parse(Math.Round(decimal.Parse(o.ToString()), 3).ToString());
                }
                else { return 0; }
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return 0;
            }
        }

        private long convertToLong(object o)
        {
            try
            {
                if (o != null)
                {
                    return long.Parse(o.ToString());
                }
                else { return 0; }
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return 0;
            }
        }
        /*************Payment Gateway Pre & Post Actions************/

        public string GetShippingStatusBody(string OrderID, string shipdate, string CourServProv, string CourTrackNo, string CourTrackLink, double dCourierCost, int Status, ref string email_id)
        {

            try
            {
                string mailBody = string.Empty;
                string cart_data = "";
                //mongo_cart_data cart_data = new mongo_cart_data();
                double shipmentvalue = 0;
                double discount = 0;
                double finalamt = 0;
                double paidamt = 0;
                string vouchers = "";
                double points = 0;
                double pointamt = 0;
                double voucheramt = 0;
                double PromoOffer = 0;
                string discount_rule = "";
                string discount_code = "";
                string strShippingInfo = "";
                string strStoreName = "";
                string strLogo = "";

                string strMobileNo = "";
                string ShippingName = "";
                string ShippingAddress = "";
                string ShippingState = "";
                string ShippingCity = "";
                string ShippingPin = "";
                string shippingAmount = "";
                string ShippingThreshold = "0";// ConfigurationSettings.AppSettings["shipping_threshold"].ToString();
                string strCompanyUrl = "";
                shippingAmount = "0";// ConfigurationSettings.AppSettings["shipping_amount"].ToString();

                //DataTable dt = GetDataFromOrder(Convert.ToInt64(OrderID));
                List<mongo_order> dt = GetDataFromOrder(Convert.ToInt64(OrderID));
                mongo_shipping_address dtShip = GetDataFromUserID(Convert.ToInt64(OrderID));

                if (dt == null || dt.Count == 0)
                    return "No Such order";

                if (dtShip == null)
                {
                    return "No Shipping Information";
                }
                else
                {
                    ShippingName = convertToString(dtShip.name);
                    ShippingAddress = convertToString(dtShip.address);
                    ShippingCity = convertToString(dtShip.city);
                    ShippingState = convertToString(dtShip.state);
                    ShippingPin = convertToString(dtShip.pincode);
                    strMobileNo = convertToString(dtShip.mobile_number);
                }


                for (int i = 0; i < dt.Count; i++)
                {
                    email_id = convertToString(dt[i].user_id);
                    //  cart_data = dt[i].cart_data.cart_data;
                    paidamt = convertToDouble(dt[i].paid_amount);
                    points = convertToDouble(dt[i].points); ;
                    pointamt = convertToDouble(dt[i].points_amount); ;
                    vouchers = convertToString(dt[i].egift_vou_no);
                    voucheramt = convertToDouble(dt[i].egift_vou_amt);
                    strShippingInfo = convertToString(dt[i].shipping_info);
                    strStoreName = convertToString(dt[i].store).ToString().Trim();
                    if (dt[i].discount_code != null)
                    {
                        discount_code = convertToString(dt[i].discount_code);
                    }
                    break;
                }


                if (discount_code != null || discount_code != "")
                {

                    List<mongo_discount_codes> dtDiscInfo = GetDiscountCodeInfo(discount_code);

                    if (dtDiscInfo == null || dtDiscInfo.Count > 0)
                    {
                        for (int p = 0; p < dtDiscInfo.Count; p++)
                        {
                            discount_rule = convertToString(dtDiscInfo[0].rule);
                        }

                    }

                    mongo_discount_rule odrulelist = JsonConvert.DeserializeObject<mongo_discount_rule>(discount_rule);

                    if (odrulelist != null)
                    {
                        PromoOffer = convertToDouble(odrulelist.discount_value);
                    }
                }


                //DataTable dtCompany = GetCompanyInfo(strStoreName);

                category_repository ocr = new category_repository();

                var dtCompany = ocr.get_company_details(strStoreName);


                if (dtCompany != null && dtCompany.Count > 0)
                {
                    strLogo = convertToString(dtCompany[0].logo);
                    strCompanyUrl = convertToString(dtCompany[0].root_url_1);
                    strMailSubCompany = convertToString(dtCompany[0].displayname);

                    if (dtCompany[0].shipping_charge == null)
                        shippingAmount = "0";
                    else
                        shippingAmount = convertToString(dtCompany[0].shipping_charge);

                    if (dtCompany[0].min_order_for_free_shipping == null)
                        ShippingThreshold = "0";
                    else
                        ShippingThreshold = convertToString(dtCompany[0].min_order_for_free_shipping);

                    if (shippingAmount == "")
                        shippingAmount = "0";

                    if (ShippingThreshold == "")
                        ShippingThreshold = "0";


                }



                //if (cart_data.Length == 0)
                if (cart_data == null)
                    return "Invalid Order";


                List<cart_item> ordlist = JsonConvert.DeserializeObject<List<cart_item>>(cart_data.ToString());

                mailBody += "<html>";
                mailBody += "<head>";
                mailBody += "</head>";
                mailBody += "<body style='padding: 0px; margin: 0px; background: #fff; text-align: center; padding: 10px; font-family: 'Trebuchet MS', Helvetica, sans-serif; font-size: 12px'>";
                mailBody += "<table cellpadding='0' cellspacing='0' width='580' align='center' style='border-left: 1px solid #dbdbdb; border-right: 1px solid #dbdbdb; border-bottom: 1px solid #dbdbdb'>";
                mailBody += "    <tr>";
                mailBody += "        <td style='background: #428bca'>";
                mailBody += "            <div style=' -webkit-box-shadow: 0px 3px 6px rgba(50, 50, 50, 0.4); -moz-box-shadow:    0px 3px 6px rgba(50, 50, 50, 0.4);";
                mailBody += "box-shadow: 0px 3px 6px rgba(50, 50, 50, 0.4);'>";
                mailBody += "<div style='padding: 5px;'>";
                mailBody += "	            <table cellpadding='0' cellspacing='0' width='100%' >";
                mailBody += "		            <tr>";
                mailBody += "			            <td style='width: 76px; padding-right: 10px;' ><img src='" + strLogo + "' /></td>";
                mailBody += "			            <td style='width: 48px; padding-left: 10px; border-left:1px solid #999999' ><img src='img/emails/eselect_logo.jpg' /></td>";
                mailBody += "			            <td style='width: 300px;;'></td>";
                mailBody += "			            <td style='width: 35px;' ><a href='https://www.facebook.com/annectosindia'><img src='img/emails/facebook.png' /></a></td>";
                mailBody += "			            <td style='width: 35px;' ><a href='https://twitter.com/annectos'><img src='img/emails/twitter.png' /></a></td>";
                mailBody += "			            <td style='width: 35px;'><a href='http://www.linkedin.com/company/annecto-rewards-&-retail-pvt-ltd-/'><img src='img/emails/linkedin.png' /></a></td>";
                mailBody += "		            </tr>";
                mailBody += "		            </table>";
                mailBody += "		            </div>";
                mailBody += "		            <div style='background: #f5c405; line-height:15px;  font-style: italic; font-size:12px; padding: 5px;'>";
                mailBody += "			            <table cellpadding='0' cellspacing='0'  >";
                mailBody += "		            <tr>";
                mailBody += "		            <td> <img src='img/emails/shiped.png' /></td>";
                mailBody += "		            <td style='font-size: 12px; font-style:italic; padding-left: 10px;'></td>";
                mailBody += "		            </tr>";
                mailBody += "		            </table>";
                mailBody += "		            </div>";
                mailBody += "            </div>";
                mailBody += "        </td>";
                mailBody += "    </tr>";
                mailBody += "    <tr>";
                mailBody += "        <td style='background: #fff; padding: 10px; font-size: 13px'>";
                mailBody += "        Hi " + ShippingName + ", <br /><br />";
                mailBody += "";
                mailBody += "        We would like to inform you that your " + strCompanyUrl + " order has been shipped from our warehouse. You will receive your order over the next few days.<br /><br />";
                mailBody += "        <div style='font-size: 12px; color: #333'>";
                mailBody += "        <b>Shipping Details:</b>    <br />";
                mailBody += "        Order No : " + OrderID + "<br />";

                /*
                mailBody += "        Shipment ID : " + OrderID + "  <br />";
                mailBody += "        Courier Service Provider : annectos logistics<br />";    
                mailBody += "        Tracking Number : AW0001022536 <br />     <br />"; 
                 */



                mailBody += "        Shipping Date : " + shipdate + "  <br />";
                mailBody += "        Courier Service Provider : " + CourServProv + "  <br />";
                mailBody += "        Courier Track No : " + CourTrackNo + "           <br />";
                mailBody += "        Courier Track Link : " + CourTrackLink + "       <br />";
                //mailBody += "        Courier Cost : " + dCourierCost + "       <br />";




                mailBody += "        <b>Shipping Address:</b>    <br />";
                mailBody += "        " + ShippingName + " <br />";
                mailBody += "        " + ShippingAddress + " <br />";
                mailBody += "        " + ShippingCity + "<br />    ";
                mailBody += "        " + ShippingState + " - " + ShippingPin + "<br />";
                mailBody += "        Mobile No : " + strMobileNo + "<br />     <br /> ";
                mailBody += "        </div>";
                mailBody += "        </td>";
                mailBody += "        </tr>";
                mailBody += "        <tr>";
                mailBody += "        <td style='background: #fff; padding: 0px 10px;  font-size: 16px;color:#3b3a3a; font-weight:bold '>";
                mailBody += "        <div style='border-top:1px dotted #cccccc; border-bottom:1px dotted #cccccc; padding-top: 5px; padding-bottom: 5px;'>";
                mailBody += "        Order No. #" + OrderID;
                mailBody += "        </div>";
                mailBody += "        </td>";
                mailBody += "        </tr>";
                mailBody += "    <tr>";
                mailBody += "        <td style='background: #fff; padding: 10px; font-size: 13px'>";
                mailBody += "        <table cellpadding='0' cellspacing='0' width='100%' >";

                int k = 0;
                //Loop for Product - Start
                /*       foreach (cart_item ord in ordlist)
                       {
                           k = k + 1;

                           mailBody += "            <tr>";
                           mailBody += "             <td colspan='6' style='color: #333333; padding-top:10px; padding-bottom:10px; font-size:13px; font-weight:bold' >" + k.ToString() + " Product Code: " + ord.sku + " - " + ord.name + "</td>	";
                           mailBody += "            </tr>";


                           mailBody += "            <tr>";
                           mailBody += "             <td style='text-align: center; padding-left: 10px; padding-right: 10px;'  ><img src='" + ord.image.thumb_link + "' /></td>	";
                           mailBody += "             <td style='color: #666666; font-size: 13px; padding-left: 10px; padding-right: 10px; text-align: center;'>Size <div style='margin-top: 10px; color: #333333'>#Size</div></td>";
                           mailBody += "             <td style='color: #666666; font-size: 13px;  text-align: center;'><div style=' border-left: 1px solid #ccc; padding-left: 10px; padding-right: 10px;'>Quantity <div style='margin-top: 10px; color: #333333; '>" + ord.quantity + "</div></div></td>";
                           mailBody += "              <td style='color: #666666; font-size: 13px; text-align: center;'><div style=' border-left: 1px solid #ccc; padding-left: 10px; padding-right: 10px;'>Unit Price <div style='margin-top: 10px; color: #333333'>" + ord.mrp + "</div></div></td>";
                           mailBody += "              <td style='color: #666666; font-size: 13px;  text-align: center;'><div style=' border-left: 1px solid #ccc; padding-left: 10px; padding-right: 10px;'>Discount <div style='margin-top: 10px; color: #ff0000'>" + ord.discount + "</div></div></td> ";
                           mailBody += "              <td style='color: #666666; font-size: 13px;  text-align: center;'> <div style=' border-left: 1px solid #ccc; padding-left: 10px; padding-right: 10px;'>Sub Total <div style='margin-top: 10px; color: #333333'>" + ord.final_offer + "</div></div></td>";
                           mailBody += "            </tr>";

                           shipmentvalue = shipmentvalue + Convert.ToDouble(ord.final_offer);
                           discount = discount + Convert.ToDouble(ord.discount);
                           finalamt = shipmentvalue;
                       } */
                if (finalamt > Convert.ToDouble(ShippingThreshold))
                {
                    shippingAmount = "Free";
                }
                else
                    finalamt = finalamt + Convert.ToDouble(shippingAmount);




                //Loop for Product - End 
                /*
                mailBody += "            <tr>";
                mailBody += "             <td colspan='6' style='color: #333333; padding-top:10px; padding-bottom:10px; font-size:13px; font-weight:bold' >2. Product Code: 00328 - MeeMee Baby Carrier (Black)</td>	";
                mailBody += "            </tr>";
            
                mailBody += "            <tr>";
                mailBody += "             <td style='text-align: center; padding-left: 10px; padding-right: 10px;'  ><img src='img/emails/product2.png' /></td>	";
                mailBody += "             <td style='color: #666666; font-size: 13px; padding-left: 10px; padding-right: 10px; text-align: center;'>Size <div style='margin-top: 10px; color: #333333'>-</div></td>";
                mailBody += "             <td style='color: #666666; font-size: 13px;  text-align: center;'><div style=' border-left: 1px solid #ccc; padding-left: 10px; padding-right: 10px;'>Quantity <div style='margin-top: 10px; color: #333333; '>1</div></div></td>";
                mailBody += "              <td style='color: #666666; font-size: 13px; text-align: center;'><div style=' border-left: 1px solid #ccc; padding-left: 10px; padding-right: 10px;'>Unit Price <div style='margin-top: 10px; color: #333333'>1200 </div></div></td>";
                mailBody += "              <td style='color: #666666; font-size: 13px;  text-align: center;'><div style=' border-left: 1px solid #ccc; padding-left: 10px; padding-right: 10px;'>Discount <div style='margin-top: 10px; color: #ff0000'>201</div></div></td>";
                mailBody += "              <td style='color: #666666; font-size: 13px;  text-align: center;'> <div style=' border-left: 1px solid #ccc; padding-left: 10px; padding-right: 10px;'>Sub Total <div style='margin-top: 10px; color: #333333'>999</div></div></td>";
                mailBody += "            </tr>";
                 */
                mailBody += "            <tr>";
                mailBody += "	            <td style='text-align: right; padding:10px 0px;' colspan='5' >";
                mailBody += "	            <div style='border-top: 1px dotted #ccc; width:100%; float:left;  padding:5px 0px '>";
                mailBody += "	            <div style='float: right; text-align:left'>";
                mailBody += "		            <div style='font-weight: bold; padding-right:20px; font-size:12px; color:#333333; '>Shipment Value </div>";
                mailBody += "		            <div style='font-weight: bold; padding-right:20px; font-size:12px; color:#333333; '>Discount   </div> ";
                mailBody += "		            <div style='font-weight: bold; padding-right:20px; font-size:12px; color:#333333; '>Shipping   </div>";
                mailBody += "		            <div style='font-weight: bold; padding-right:20px; font-size:12px; color:#333333; '>Points Redeemed   </div>";
                mailBody += "		            <div style='font-weight: bold; padding-right:20px; font-size:12px; color:#333333; '>Gift Voucher    </div>";
                mailBody += "		            <div style='font-weight: bold; padding-right:20px; font-size:12px; color:#333333; '>Promo Code    </div>";
                mailBody += "		            <div style='font-weight: bold; padding-right:20px; font-size:12px; color:#333333; '>Cash/Card Paid  </div>";
                mailBody += "		            </div>";
                mailBody += "	            </div>";
                mailBody += "	            </td>";
                mailBody += "	            <td style='text-align: right'>";
                mailBody += "	            <div style='border-top: 1px dotted #ccc; padding:5px 0px '>";
                mailBody += "	            <div style='font-weight: bold; padding-right:20px; font-size:12px; color:#333333; '>" + shipmentvalue.ToString() + "</div>";
                mailBody += "	            <div style='font-weight: bold; padding-right:20px; font-size:12px; color:#ff0000; '>" + discount.ToString() + "</div>";
                mailBody += "	            <div style='font-weight: bold; padding-right:20px; font-size:12px; color:#038203; '>" + shippingAmount.ToString() + "</div>";
                mailBody += "	            <div style='font-weight: bold; padding-right:20px; font-size:12px; color:#038203; '>" + points.ToString() + "</div>";
                mailBody += "	            <div style='font-weight: bold; padding-right:20px; font-size:12px; color:#038203; '>" + voucheramt.ToString() + "</div>";
                mailBody += "	            <div style='font-weight: bold; padding-right:20px; font-size:12px; color:#038203; '>" + PromoOffer.ToString() + "</div>";
                mailBody += "	            <div style='font-weight: bold; padding-right:20px; font-size:12px; color:#038203; '>" + paidamt.ToString() + "</div>";
                mailBody += "	            </div>";
                mailBody += "	            </td>";
                mailBody += "            </tr>";
                mailBody += "            <tr>";
                mailBody += "	            <td valign='top' colspan='5' >";
                mailBody += "	            <div style='float: right; text-align:left; border-top: 1px dotted #ccc; width: 110px; padding-left: 40px; padding-top:5px;'>";
                mailBody += "	            <div style='font-weight: bold; padding-right:20px; font-size:12px; color:#333333; '>Amount paid   </div>";
                mailBody += "	            </div>";
                mailBody += "	            </td>";
                mailBody += "	            <td  valign='top'  style='text-align: right'>";
                mailBody += "	            <div style='border-top: 1px dotted #ccc; padding:5px 0px '>";
                mailBody += "	            <div style='font-weight: bold; padding-right:20px; font-size:12px; color:#333333; '>" + finalamt.ToString() + "</div>";
                mailBody += "	            </div>";
                mailBody += "	            </td>";
                mailBody += "            </tr>";
                /*
                mailBody += "            <tr>";
                mailBody += "	            <td valign='top' style='font-size: 12px; padding-top:20px; color: #333333' colspan='6' >";
                mailBody += "	            <b>Shipping Address:</b>    <br />";
                mailBody += ShippingName + " <br />    ";
                mailBody += ShippingAddress + ",  <br />    ";
                mailBody += ShippingCity + " <br />    ";
                mailBody += ShippingState + " - " + ShippingPin + " <br />     <br />";
                //mailBody += "You can track or manage your order at any time by going to <a href='#' style='color: #41a0ff; text-decoration:none'>http://www.annectos.in/myaccounts.php?view=myorders</a>. If you need any assistance or have any questions, feel free to contact us by using the web form at <a href='#' style='color: #41a0ff; text-decoration:none'>http://www.annectos.in/contactus</a> or call us at +91 9686202046 | +91 9972334590";
                mailBody += "If you need any assistance or have any questions, feel free to contact us by using the web form at <a href='#' style='color: #41a0ff; text-decoration:none'>http://www.annectos.in/contactus</a> or call us at +91 9686202046 | +91 9972334590";
                mailBody += "	            </td>";
                mailBody += "	            </tr>";
                */
                mailBody += "        </table>";
                mailBody += "        </td>";
                mailBody += "        </tr>";
                mailBody += "<tr>";
                mailBody += "        <td style='background: #fff; padding: 10px; font-size: 13px'>";

                //Promotion/Marketting
                /*
                mailBody += "        <div style='font-size: 18px; font-weight:bold; color:#333333; text-transform:uppercase; line-height:30px; border-bottom: 1px solid #ccc'>You may also consider</div>";
                mailBody += "        <div style='padding-top: 10px'>";
                mailBody += "            <table cellspacing='0' cellpadding='5' style='width: 100%'>";
                mailBody += "	            <tr>";
                mailBody += "		            <td align='left' style='width: 33.3%; padding:0px 10px'>";
                mailBody += "			            <div><img src='img/emails/offer1.png' /></div>";
                mailBody += "		            </td>";
                mailBody += "		            <td align='left' style='width: 33.3%; padding:0px 10px'>";
                mailBody += "			            <div><img src='img/emails/offer2.png' /></div>";
                mailBody += "		            </td>";
                mailBody += "		            <td align='left' style='width: 33.3%; padding:0px 10px'>";
                mailBody += "			            <div><img src='img/emails/offer3.png' /></div>";
                mailBody += "		            </td>";
                mailBody += "	            </tr>";
                mailBody += "	            <tr>";
                mailBody += "		            <td align='left' style='width: 33.3%'>";
                mailBody += "			            <div style='font-size: 13px; color:#333; font-weight: bold; padding-left: 10px;'>Rooptex Kurthi</div>";
                mailBody += "		            </td>";
                mailBody += "		            <td align='left' style='width: 33.3%'>";
                mailBody += "			            <div style='font-size: 13px; color:#333; font-weight: bold; padding-left: 10px;'>Grass Black Crew Neck ";
                mailBody += "Sporty T Shirt</div>";
                mailBody += "		            </td>";
                mailBody += "		            <td align='left' style='width: 33.3%'>";
                mailBody += "			            <div style='font-size: 13px; color:#333; font-weight: bold; padding-left: 10px;'>US Polo Assn Blue Shirt</div>";
                mailBody += "		            </td>";
                mailBody += "	            </tr>";
                mailBody += "	            <tr>";
                mailBody += "		            <td  align='left' style='width: 33.3%; '>";
                mailBody += "			            <div style='font-size: 12px; color:#333; border-top:1px dotted #ccc;  padding:5px 10px;'>";
                mailBody += "			            <div><span style='color:#666666'>MRP: <span style='text-decoration: line-through'>1790</span></span> | 60% off </div>";
                mailBody += "			            <div style='color: #ff6600'>Offer: 758 <span style='color: #333'>|</span> Points: 758 </div>";
                mailBody += "			            </div>";
                mailBody += "		            </td>";
                mailBody += "		            <td  align='left' style='width: 33.3%; '>";
                mailBody += "			            <div style='font-size: 12px; color:#333; border-top:1px dotted #ccc;  padding:5px 10px;'>";
                mailBody += "			            <div><span style='color:#666666'>MRP: <span style='text-decoration: line-through'>1790</span></span> | 60% off </div>";
                mailBody += "			            <div style='color: #ff6600'>Offer: 758 <span style='color: #333'>|</span> Points: 758 </div>";
                mailBody += "			            </div>";
                mailBody += "		            </td>";
                mailBody += "		            <td  align='left' style='width: 33.3%; '>";
                mailBody += "			            <div style='font-size: 12px; color:#333; border-top:1px dotted #ccc;  padding:5px 10px;'>";
                mailBody += "			            <div><span style='color:#666666'>MRP: <span style='text-decoration: line-through'>1790</span></span> | 60% off </div>";
                mailBody += "			            <div style='color: #ff6600'>Offer: 758 <span style='color: #333'>|</span> Points: 758 </div>";
                mailBody += "			            </div>";
                mailBody += "		            </td>";
                mailBody += "	            </tr>";
                mailBody += "            </table>";
                mailBody += "        </div>";
                 */
                mailBody += "        </td>";
                mailBody += "</tr>";
                /*mailBody += "<tr>";
                mailBody += "        <td style='background: #fff; padding: 10px; font-size: 13px'>";
                mailBody += "        <table style='width: 100%' cellpadding='0' cellspacing='0'>";
                mailBody += "            <tr>";
                mailBody += "	            <td align='left'><a href='#'><img src='img/emails/add1.png' /></a></td>";
                mailBody += "	            <td align='right'><a href='#'><img src='img/emails/add2.png' /></a></td>";
                mailBody += "            </tr>";
                mailBody += "        </table>";
                mailBody += "        </td>";
                mailBody += "        </tr>";*/
                mailBody += "</table>";
                /*mailBody += "<table cellpadding='0' cellspacing='0' width='500' align='center'  style=' font-size: 12px; color: #666666'>";
                mailBody += "    <tr>";
                mailBody += "        <td style='padding-top: 10px;' align='center'>customerfirst@annectos.in   call us at +91 9686202046 | +91 9972334590";
                mailBody += "</td>";
                mailBody += "    </tr>";
                mailBody += "    <tr>";
                mailBody += "        <td style='padding-top: 10px; vertical-align: top;' align='center'>Connect With Us <a href='#'><img width='18' height='18' src='img/emails/facebook.png' /></a>  <a href='#'><img width='18' height='18' src='img/emails/twitter.png' /></a>  <a href='#'><img width='18' height='18' src='img/emails/linkedin.png' /></a>";
                mailBody += "</td>";
                mailBody += "    </tr>";
                mailBody += "    <tr>";
                mailBody += "        <td style='padding-top: 10px; font-size: 10px;' align='center'>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. All Rights Reserved. www.annectos.in";
                mailBody += "</td>";
                mailBody += "    </tr>";
                mailBody += "</table>";*/
                mailBody += "</body>";
                mailBody += "</html>";


                mailBody = mailBody.Replace("img/emails/", ConfigurationSettings.AppSettings["email_img"].ToString() + "/img/emails/");
                return mailBody;
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return "";
            }
        }

        public string GetDelayStatusBody(string OrderID, string delay_reason, string DelayDays, int Status, ref string email_id)
        {

            try
            {
                string strBodyEmail = string.Empty;


                string strMobileNo = "";
                string ShippingName = "";
                string ShippingAddress = "";
                string ShippingState = "";
                string ShippingCity = "";
                string ShippingPin = "";

                string strLogo = "";
                string strCompanyUrl = "";
                string strStoreName = "";

                //DataTable dt = GetDataFromOrder(Convert.ToInt64(OrderID));

                List<mongo_order> dt = GetDataFromOrder(Convert.ToInt64(OrderID));

                mongo_shipping_address dtShip = GetDataFromUserID(Convert.ToInt64(OrderID));

                if (dt == null || dt.Count == 0)
                    return "No Such order";

                for (int i = 0; i < dt.Count; i++)
                {
                    email_id = convertToString(dt[i].user_id);
                    strStoreName = convertToString(dt[i].store).ToString().Trim();
                    break;
                }

                if (dtShip == null)
                {
                    return "No Shipping Information";
                }
                else
                {
                    ShippingName = convertToString(dtShip.name);
                    ShippingAddress = convertToString(dtShip.address);
                    ShippingCity = convertToString(dtShip.city);
                    ShippingState = convertToString(dtShip.state);
                    ShippingPin = convertToString(dtShip.pincode);
                    strMobileNo = convertToString(dtShip.mobile_number);

                }

                //DataTable dtCompany = GetCompanyInfo(strStoreName);
                category_repository ocr = new category_repository();
                var dtCompany = ocr.get_company_details(strStoreName);

                if (dtCompany != null && dtCompany.Count > 0)
                {
                    strLogo = convertToString(dtCompany[0].logo);
                    strCompanyUrl = convertToString(dtCompany[0].root_url_1);
                }




                strBodyEmail += "<html>";
                strBodyEmail += "<head>";
                strBodyEmail += "</head>";
                strBodyEmail += "<body style='padding: 0px; margin: 0px; background: #fff; text-align: center; padding: 10px; font-family: 'Trebuchet MS', Helvetica, sans-serif; font-size: 12px'>";
                strBodyEmail += "<table cellpadding='0' cellspacing='0' width='580' align='center' style='border-left: 1px solid #dbdbdb; border-right: 1px solid #dbdbdb; border-bottom: 1px solid #dbdbdb'>";
                strBodyEmail += "    <tr>";
                strBodyEmail += "        <td style='background: #f5f5f5'>";
                strBodyEmail += "            <div style=' -webkit-box-shadow: 0px 3px 6px rgba(50, 50, 50, 0.4); -moz-box-shadow:    0px 3px 6px rgba(50, 50, 50, 0.4);";
                strBodyEmail += "box-shadow: 0px 3px 6px rgba(50, 50, 50, 0.4);'>";
                strBodyEmail += "<div style='padding: 5px;'>";
                strBodyEmail += "	            <table cellpadding='0' cellspacing='0' width='100%' >";
                strBodyEmail += "		            <tr>";
                strBodyEmail += "			            <td style='width: 76px; padding-right: 10px;' ><img src='" + strLogo + "' /></td>";
                strBodyEmail += "			            <td style='width: 48px; padding-left: 10px; border-left:1px solid #999999' ><img src='img/emails/eselect_logo.jpg' /></td>";
                strBodyEmail += "			            <td style='width: 300px;;'></td>";
                strBodyEmail += "			            <td style='width: 35px;' ><a href='https://www.facebook.com/annectosindia'><img src='img/emails/facebook.png' /></a></td>";
                strBodyEmail += "			            <td style='width: 35px;' ><a href='https://twitter.com/annectos'><img src='img/emails/twitter.png' /></a></td>";
                strBodyEmail += "			            <td style='width: 35px;'><a href='http://www.linkedin.com/company/annecto-rewards-&-retail-pvt-ltd-/'><img src='img/emails/linkedin.png' /></a></td>";
                strBodyEmail += "		            </tr>";
                strBodyEmail += "		            </table>";
                strBodyEmail += "		            </div>";
                strBodyEmail += "		            <div style='background: #f5c405; line-height:15px;  font-style: italic; font-size:12px; padding: 5px;'>";
                strBodyEmail += "			            <table cellpadding='0' cellspacing='0'  >";
                strBodyEmail += "		            <tr>";
                strBodyEmail += "		            <td> <img src='img/emails/shiped.png' /></td>";
                strBodyEmail += "		            <td style='font-size: 12px; font-style:italic; padding-left: 10px;'></td>";
                strBodyEmail += "		            </tr>";
                strBodyEmail += "		            </table>";
                strBodyEmail += "		            </div>";
                strBodyEmail += "            </div>";
                strBodyEmail += "        </td>";
                strBodyEmail += "    </tr>";
                strBodyEmail += "    <tr>";
                strBodyEmail += "        <td style='background: #fff; padding: 10px; font-size: 13px'>";
                strBodyEmail += "        Hi " + ShippingName + ", <br /><br />";
                strBodyEmail += "";
                strBodyEmail += "        <div style='font-size: 12px; color: #333'>";
                strBodyEmail += "Your order number :" + OrderID + " has been delayed by " + DelayDays + " days. <br />";
                strBodyEmail += "Reason : " + delay_reason + "<br />";
                strBodyEmail += "<br/>";
                strBodyEmail += "Our customer service representative will call you within the next business day to update you on the order status.";
                strBodyEmail += "<br/>";
                strBodyEmail += "If you have any questions, please feel free to contact us at customerfirst@annectos.in or by phone at +91 9686202046 /9972334590 (10 am - 7 pm, Mon – Fri)";
                strBodyEmail += "<br/>";
                strBodyEmail += "Regards,";
                strBodyEmail += "<br/>";
                strBodyEmail += "<br/>";
                strBodyEmail += "Annectos Support Team";
                strBodyEmail += "</div>";
                strBodyEmail += "</td>";
                strBodyEmail += "</tr>";

                strBodyEmail += "<tr>";
                strBodyEmail += "<td style='background: #fff; padding: 10px; font-size: 13px'>";
                strBodyEmail += "</td>";
                strBodyEmail += "</tr>";

                strBodyEmail += "</table>";

                strBodyEmail += "</body>";
                strBodyEmail += "</html>";

                strBodyEmail = strBodyEmail.Replace("img/emails/", ConfigurationSettings.AppSettings["email_img"].ToString() + "/img/emails/");
                return strBodyEmail;
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return "";
            }

        }

        public string GetCancelStatusBody(string OrderID, string cancel_reason, int Status, ref string email_id)
        {

            try
            {
                string strBodyEmail = string.Empty;

                string strMobileNo = "";
                string ShippingName = "";
                string ShippingAddress = "";
                string ShippingState = "";
                string ShippingCity = "";
                string ShippingPin = "";

                string strLogo = "";
                string strCompanyUrl = "";
                string strStoreName = "";

                //DataTable dt = GetDataFromOrder(Convert.ToInt64(OrderID));
                List<mongo_order> dt = GetDataFromOrder(Convert.ToInt64(OrderID));
                mongo_shipping_address dtShip = GetDataFromUserID(Convert.ToInt64(OrderID));

                if (dt == null || dt.Count == 0)
                    return "No Such order";

                for (int i = 0; i < dt.Count; i++)
                {
                    email_id = convertToString(dt[i].user_id);
                    strStoreName = convertToString(dt[i].store).Trim();
                    break;
                }

                if (dtShip == null)
                {
                    return "No Shipping Information";
                }
                else
                {
                    ShippingName = convertToString(dtShip.name);
                    ShippingAddress = convertToString(dtShip.address);
                    ShippingCity = convertToString(dtShip.city);
                    ShippingState = convertToString(dtShip.state);
                    ShippingPin = convertToString(dtShip.pincode);
                    strMobileNo = convertToString(dtShip.mobile_number);

                }

                //DataTable dtCompany = GetCompanyInfo(strStoreName);
                category_repository ocr = new category_repository();
                var dtCompany = ocr.get_company_details(strStoreName);

                if (dtCompany != null && dtCompany.Count > 0)
                {
                    strLogo = convertToString(dtCompany[0].logo);
                    strCompanyUrl = convertToString(dtCompany[0].root_url_1);

                }


                strBodyEmail += "<html>";
                strBodyEmail += "<head>";
                strBodyEmail += "</head>";
                strBodyEmail += "<body style='padding: 0px; margin: 0px; background: #fff; text-align: center; padding: 10px; font-family: 'Trebuchet MS', Helvetica, sans-serif; font-size: 12px'>";
                strBodyEmail += "<table cellpadding='0' cellspacing='0' width='580' align='center' style='border-left: 1px solid #dbdbdb; border-right: 1px solid #dbdbdb; border-bottom: 1px solid #dbdbdb'>";
                strBodyEmail += "    <tr>";
                strBodyEmail += "        <td style='background: #f5f5f5'>";
                strBodyEmail += "            <div style=' -webkit-box-shadow: 0px 3px 6px rgba(50, 50, 50, 0.4); -moz-box-shadow:    0px 3px 6px rgba(50, 50, 50, 0.4);";
                strBodyEmail += "box-shadow: 0px 3px 6px rgba(50, 50, 50, 0.4);'>";
                strBodyEmail += "<div style='padding: 5px;'>";
                strBodyEmail += "	            <table cellpadding='0' cellspacing='0' width='100%' >";
                strBodyEmail += "		            <tr>";
                strBodyEmail += "			            <td style='width: 76px; padding-right: 10px;' ><img src='" + strLogo + "' /></td>";
                strBodyEmail += "			            <td style='width: 48px; padding-left: 10px; border-left:1px solid #999999' ><img src='img/emails/eselect_logo.jpg' /></td>";
                strBodyEmail += "			            <td style='width: 300px;;'></td>";
                strBodyEmail += "			            <td style='width: 35px;' ><a href='https://www.facebook.com/annectosindia'><img src='img/emails/facebook.png' /></a></td>";
                strBodyEmail += "			            <td style='width: 35px;' ><a href='https://twitter.com/annectos'><img src='img/emails/twitter.png' /></a></td>";
                strBodyEmail += "			            <td style='width: 35px;'><a href='http://www.linkedin.com/company/annecto-rewards-&-retail-pvt-ltd-/'><img src='img/emails/linkedin.png' /></a></td>";
                strBodyEmail += "		            </tr>";
                strBodyEmail += "		            </table>";
                strBodyEmail += "		            </div>";
                strBodyEmail += "		            <div style='background: #1b6bb9; line-height:15px;  font-style: italic; font-size:12px; padding: 5px;'>";
                strBodyEmail += "			            <table cellpadding='0' cellspacing='0'  >";
                strBodyEmail += "		            <tr>";
                strBodyEmail += "		            <td> <img src='img/emails/shiped.png' /></td>";
                strBodyEmail += "		            <td style='font-size: 12px; font-style:italic; padding-left: 10px;'></td>";
                strBodyEmail += "		            </tr>";
                strBodyEmail += "		            </table>";
                strBodyEmail += "		            </div>";
                strBodyEmail += "            </div>";
                strBodyEmail += "        </td>";
                strBodyEmail += "    </tr>";
                strBodyEmail += "    <tr>";
                strBodyEmail += "        <td style='background: #fff; padding: 10px; font-size: 13px'>";
                strBodyEmail += "        Hi " + ShippingName + ", <br /><br />";
                strBodyEmail += "";
                strBodyEmail += "        <div style='font-size: 12px; color: #333'>";
                strBodyEmail += "<br/>";
                strBodyEmail += "Your order number : " + OrderID + " has been canceled. <br />";
                strBodyEmail += "Reason : " + cancel_reason + "<br />";
                strBodyEmail += "<br/>";
                strBodyEmail += "Our customer service representative will call you within the next business day to update you on the order status.";
                strBodyEmail += "<br/>";
                strBodyEmail += "If you have any questions, please feel free to contact us at customerfirst@annectos.in or by phone at +91 9686202046 /9972334590 (10 am - 7 pm, Mon – Fri)";
                strBodyEmail += "<br/>";
                strBodyEmail += "Regards,";
                strBodyEmail += "<br/>";
                strBodyEmail += "<br/>";
                strBodyEmail += "Annectos Support Team";
                strBodyEmail += "</div>";
                strBodyEmail += "</td>";
                strBodyEmail += "</tr>";

                strBodyEmail += "<tr>";
                strBodyEmail += "<td style='background: #fff; padding: 10px; font-size: 13px'>";
                strBodyEmail += "</td>";
                strBodyEmail += "</tr>";

                strBodyEmail += "</table>";

                strBodyEmail += "</body>";
                strBodyEmail += "</html>";


                strBodyEmail = strBodyEmail.Replace("img/emails/", ConfigurationSettings.AppSettings["email_img"].ToString() + "/img/emails/");

                return strBodyEmail;
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return "";
            }


        }

        public string GetOrderTrackBody(string strStoreName)
        {

            try
            {
                string strBodyEmail = string.Empty;


                string strLogo = "";
                string strCompanyUrl = "";

                //DataTable dtCompany = GetCompanyInfo(strStoreName);

                category_repository ocr = new category_repository();
                var dtCompany = ocr.get_company_details(strStoreName);

                if (dtCompany != null && dtCompany.Count > 0)
                {
                    strLogo = convertToString(dtCompany[0].logo);
                    strCompanyUrl = convertToString(dtCompany[0].root_url_1);

                }




                strBodyEmail += "<html>";
                strBodyEmail += "<head>";
                strBodyEmail += "</head>";
                strBodyEmail += "<body style='padding: 0px; margin: 0px; background: #fff; text-align: center; padding: 10px; font-family: 'Trebuchet MS', Helvetica, sans-serif; font-size: 12px'>";
                strBodyEmail += "<table cellpadding='0' cellspacing='0' width='580' align='center' style='border-left: 1px solid #dbdbdb; border-right: 1px solid #dbdbdb; border-bottom: 1px solid #dbdbdb'>";
                strBodyEmail += "    <tr>";
                strBodyEmail += "        <td style='background: #f5f5f5'>";
                strBodyEmail += "            <div style=' -webkit-box-shadow: 0px 3px 6px rgba(50, 50, 50, 0.4); -moz-box-shadow:    0px 3px 6px rgba(50, 50, 50, 0.4);";
                strBodyEmail += "box-shadow: 0px 3px 6px rgba(50, 50, 50, 0.4);'>";
                strBodyEmail += "<div style='padding: 5px;'>";
                strBodyEmail += "	            <table cellpadding='0' cellspacing='0' width='100%' >";
                strBodyEmail += "		            <tr>";
                strBodyEmail += "			            <td style='width: 76px; padding-right: 10px;' ><img src='" + strLogo + "' /></td>";
                strBodyEmail += "			            <td style='width: 48px; padding-left: 10px; border-left:1px solid #999999' ><img src='img/emails/eselect_logo.jpg' /></td>";
                strBodyEmail += "			            <td style='width: 300px;;'></td>";
                strBodyEmail += "			            <td style='width: 35px;' ><a href='https://www.facebook.com/annectosindia'><img src='img/emails/facebook.png' /></a></td>";
                strBodyEmail += "			            <td style='width: 35px;' ><a href='https://twitter.com/annectos'><img src='img/emails/twitter.png' /></a></td>";
                strBodyEmail += "			            <td style='width: 35px;'><a href='http://www.linkedin.com/company/annecto-rewards-&-retail-pvt-ltd-/'><img src='img/emails/linkedin.png' /></a></td>";
                strBodyEmail += "		            </tr>";
                strBodyEmail += "		            </table>";
                strBodyEmail += "		            </div>";
                strBodyEmail += "		            <div style='background: #f5c405; line-height:15px;  font-style: italic; font-size:12px; padding: 5px;'>";
                strBodyEmail += "			            <table cellpadding='0' cellspacing='0'  >";
                strBodyEmail += "		            <tr>";
                strBodyEmail += "		            <td> <img src='img/emails/shiped.png' /></td>";
                strBodyEmail += "		            <td style='font-size: 12px; font-style:italic; padding-left: 10px;'> </td>";
                strBodyEmail += "		            </tr>";
                strBodyEmail += "		            </table>";
                strBodyEmail += "		            </div>";
                strBodyEmail += "            </div>";
                strBodyEmail += "        </td>";
                strBodyEmail += "    </tr>";
                strBodyEmail += "    <tr>";
                strBodyEmail += "        <td style='background: #fff; padding: 10px; font-size: 13px'>";
                strBodyEmail += "        Hi There , <br /><br />";
                strBodyEmail += "";
                strBodyEmail += "        <div style='font-size: 12px; color: #333'>";
                strBodyEmail += "Please find the attached list of the orders which are still in Order Received Status after 24 Hours. <br />";
                strBodyEmail += "Required your attention immediately.<br />";
                strBodyEmail += "<br/>";
                strBodyEmail += "<br/>";
                strBodyEmail += "Regards,";
                strBodyEmail += "<br/>";
                strBodyEmail += "<br/>";
                strBodyEmail += "Annectos Support Team";
                strBodyEmail += "</div>";
                strBodyEmail += "</td>";
                strBodyEmail += "</tr>";

                strBodyEmail += "<tr>";
                strBodyEmail += "<td style='background: #fff; padding: 10px; font-size: 13px'>";
                strBodyEmail += "</td>";
                strBodyEmail += "</tr>";

                strBodyEmail += "</table>";

                strBodyEmail += "</body>";
                strBodyEmail += "</html>";

                strBodyEmail = strBodyEmail.Replace("img/emails/", ConfigurationSettings.AppSettings["email_img"].ToString() + "/img/emails/");
                return strBodyEmail;
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return "";
            }

        }

        public string GetWillShipOrderTrackBody(string strStoreName)
        {

            try
            {
                string strBodyEmail = string.Empty;


                string strLogo = "";
                string strCompanyUrl = "";

                //DataTable dtCompany = GetCompanyInfo(strStoreName);

                category_repository ocr = new category_repository();
                var dtCompany = ocr.get_company_details(strStoreName);


                if (dtCompany != null && dtCompany.Count > 0)
                {
                    strLogo = convertToString(dtCompany[0].logo);
                    strCompanyUrl = convertToString(dtCompany[0].root_url_1);

                }




                strBodyEmail += "<html>";
                strBodyEmail += "<head>";
                strBodyEmail += "</head>";
                strBodyEmail += "<body style='padding: 0px; margin: 0px; background: #fff; text-align: center; padding: 10px; font-family: 'Trebuchet MS', Helvetica, sans-serif; font-size: 12px'>";
                strBodyEmail += "<table cellpadding='0' cellspacing='0' width='580' align='center' style='border-left: 1px solid #dbdbdb; border-right: 1px solid #dbdbdb; border-bottom: 1px solid #dbdbdb'>";
                strBodyEmail += "    <tr>";
                strBodyEmail += "        <td style='background: #f5f5f5'>";
                strBodyEmail += "            <div style=' -webkit-box-shadow: 0px 3px 6px rgba(50, 50, 50, 0.4); -moz-box-shadow:    0px 3px 6px rgba(50, 50, 50, 0.4);";
                strBodyEmail += "box-shadow: 0px 3px 6px rgba(50, 50, 50, 0.4);'>";
                strBodyEmail += "<div style='padding: 5px;'>";
                strBodyEmail += "	            <table cellpadding='0' cellspacing='0' width='100%' >";
                strBodyEmail += "		            <tr>";
                strBodyEmail += "			            <td style='width: 76px; padding-right: 10px;' ><img src='" + strLogo + "' /></td>";
                strBodyEmail += "			            <td style='width: 48px; padding-left: 10px; border-left:1px solid #999999' ><img src='img/emails/eselect_logo.jpg' /></td>";
                strBodyEmail += "			            <td style='width: 300px;;'></td>";
                strBodyEmail += "			            <td style='width: 35px;' ><a href='https://www.facebook.com/annectosindia'><img src='img/emails/facebook.png' /></a></td>";
                strBodyEmail += "			            <td style='width: 35px;' ><a href='https://twitter.com/annectos'><img src='img/emails/twitter.png' /></a></td>";
                strBodyEmail += "			            <td style='width: 35px;'><a href='http://www.linkedin.com/company/annecto-rewards-&-retail-pvt-ltd-/'><img src='img/emails/linkedin.png' /></a></td>";
                strBodyEmail += "		            </tr>";
                strBodyEmail += "		            </table>";
                strBodyEmail += "		            </div>";
                strBodyEmail += "		            <div style='background: #f5c405; line-height:15px;  font-style: italic; font-size:12px; padding: 5px;'>";
                strBodyEmail += "			            <table cellpadding='0' cellspacing='0'  >";
                strBodyEmail += "		            <tr>";
                strBodyEmail += "		            <td> <img src='img/emails/shiped.png' /></td>";
                strBodyEmail += "		            <td style='font-size: 12px; font-style:italic; padding-left: 10px;'> </td>";
                strBodyEmail += "		            </tr>";
                strBodyEmail += "		            </table>";
                strBodyEmail += "		            </div>";
                strBodyEmail += "            </div>";
                strBodyEmail += "        </td>";
                strBodyEmail += "    </tr>";
                strBodyEmail += "    <tr>";
                strBodyEmail += "        <td style='background: #fff; padding: 10px; font-size: 13px'>";
                strBodyEmail += "        Hi There , <br /><br />";
                strBodyEmail += "";
                strBodyEmail += "        <div style='font-size: 12px; color: #333'>";
                strBodyEmail += "Please find the attached list of the orders which are still in Will Ship Status after Minimum Shipping Days. <br />";
                strBodyEmail += "Required your attention immediately.<br />";
                strBodyEmail += "<br/>";
                strBodyEmail += "<br/>";
                strBodyEmail += "Regards,";
                strBodyEmail += "<br/>";
                strBodyEmail += "<br/>";
                strBodyEmail += "Annectos Support Team";
                strBodyEmail += "</div>";
                strBodyEmail += "</td>";
                strBodyEmail += "</tr>";

                strBodyEmail += "<tr>";
                strBodyEmail += "<td style='background: #fff; padding: 10px; font-size: 13px'>";
                strBodyEmail += "</td>";
                strBodyEmail += "</tr>";

                strBodyEmail += "</table>";

                strBodyEmail += "</body>";
                strBodyEmail += "</html>";

                strBodyEmail = strBodyEmail.Replace("img/emails/", ConfigurationSettings.AppSettings["email_img"].ToString() + "/img/emails/");
                return strBodyEmail;
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return "";
            }

        }


        public string GetOrderStatusBody(string OrderID, int Status, ref string email_id)
        {

            try
            {
                string mailBody = string.Empty;
                string cart_data = "";
                // cart_data = new mongo_cart_data();
                double shipmentvalue = 0;
                double discount = 0;
                double finalamt = 0;
                double paidamt = 0;
                double totalamount = 0;
                string vouchers = "";
                double points = 0;
                double lineitemprice = 0;
                double pointamt = 0;
                double voucheramt = 0;
                double PromoOffer = 0;
                string strStoreName = "";
                string strLogo = "";
                string ShippingName = "";
                string ShippingAddress = "";
                string ShippingState = "";
                string ShippingCity = "";
                string ShippingPin = "";
                string shippingAmount = "";
                string strMobileNo = "";
                string strCompanyUrl = "";
                string strMyorderURL = "";
                string expshippingAmount = "0";
                string discount_code = "";
                string discount_rule = "";

                string ShippingThreshold = ConfigurationSettings.AppSettings["shipping_threshold"].ToString();
                shippingAmount = ConfigurationSettings.AppSettings["shipping_amount"].ToString();

                //DataTable dt = GetDataFromOrder(Convert.ToInt64(OrderID));

                List<mongo_order> dt = GetDataFromOrder(Convert.ToInt64(OrderID));

                mongo_shipping_address dtShip = GetDataFromUserID(Convert.ToInt64(OrderID));

                if (dt == null || dt.Count == 0)
                    return "No Such order";

                if (dtShip == null)
                {
                    return "No Shipping Information";
                }
                else
                {
                    ShippingName = convertToString(dtShip.name);
                    ShippingAddress = convertToString(dtShip.address);
                    ShippingCity = convertToString(dtShip.city);
                    ShippingState = convertToString(dtShip.state);
                    ShippingPin = convertToString(dtShip.pincode);
                    strMobileNo = convertToString(dtShip.mobile_number);
                }


                for (int i = 0; i < dt.Count; i++)
                {
                    email_id = convertToString(dt[i].user_id);
                    cart_data = (dt[i].cart_data.cart_data);
                    paidamt = convertToDouble(dt[i].paid_amount);
                    points = convertToDouble(dt[i].points); ;
                    pointamt = convertToDouble(dt[i].points_amount); ;
                    vouchers = convertToString(dt[i].egift_vou_no);
                    voucheramt = convertToDouble(dt[i].egift_vou_amt);

                    strStoreName = convertToString(dt[i].store).ToString().Trim();
                    if (dt[i].discount_code != null)
                    {
                        discount_code = convertToString(dt[i].discount_code);
                    }
                    break;
                }

                if (discount_code != null || discount_code != "")
                {

                    var dtDiscInfo = GetDiscountCodeInfo(discount_code);

                    if (dtDiscInfo == null || dtDiscInfo.Count > 0)
                    {
                        for (int p = 0; p < dtDiscInfo.Count; p++)
                        {
                            discount_rule = convertToString(dtDiscInfo[0].rule);
                        }

                    }

                    disc_rule odrulelist = JsonConvert.DeserializeObject<disc_rule>(discount_rule);

                    if (odrulelist != null)
                    {
                        PromoOffer = convertToDouble(odrulelist.discount_value);
                    }
                }

                //DataTable dtCompany = GetCompanyInfo(strStoreName);

                category_repository ocr = new category_repository();
                var dtCompany = ocr.get_company_details(strStoreName);
                if (dtCompany != null && dtCompany.Count > 0)
                {
                    strLogo = convertToString(dtCompany[0].logo);
                    strCompanyUrl = convertToString(dtCompany[0].root_url_1);
                    strMailSubCompany = convertToString(dtCompany[0].displayname);

                    if (dtCompany[0].shipping_charge == 0)
                        shippingAmount = "0";
                    else
                        shippingAmount = convertToString(dtCompany[0].shipping_charge);

                    if (dtCompany[0].min_order_for_free_shipping == 0)
                        ShippingThreshold = "0";
                    else
                        ShippingThreshold = convertToString(dtCompany[0].min_order_for_free_shipping);

                    if (shippingAmount == "")
                        shippingAmount = "0";

                    if (ShippingThreshold == "")
                        ShippingThreshold = "0";
                    strMyorderURL = strCompanyUrl + "index.html#/myorder";
                }

                if (cart_data == null)
                    return "Invalid Order";


                List<cart_item> ordlist = JsonConvert.DeserializeObject<List<cart_item>>(cart_data.ToString());

                //Update Stock here
                if (ordlist.Count > 0)
                {
                    //Update Discount Coupon
                    UpdateDiscountCode(email_id, discount_code);

                    ecomm.model.repository.category_repository cr = new ecomm.model.repository.category_repository();
                    foreach (cart_item ordupdt in ordlist)
                    {
                        cr.update_stock_qty(ordupdt.id, Convert.ToDouble(ordupdt.quantity), "Minus");
                    }
                }
                //End of Stock Update
                mailBody += "<html>";
                mailBody += "<head>";
                mailBody += "</head>";
                mailBody += "<body style='padding: 0px; margin: 0px; background: #fff; text-align: center; padding: 10px; font-family: 'Trebuchet MS', Helvetica, sans-serif; font-size: 12px'>";
                mailBody += "<table cellpadding='0' cellspacing='0' width='580' align='center' style='border-left: 1px solid #dbdbdb; border-right: 1px solid #dbdbdb; border-bottom: 1px solid #dbdbdb'>";
                mailBody += "    <tr>";
                mailBody += "        <td style='background: #f5f5f5'>";
                mailBody += "            <div style=' -webkit-box-shadow: 0px 3px 6px rgba(50, 50, 50, 0.4); -moz-box-shadow:    0px 3px 6px rgba(50, 50, 50, 0.4);";
                mailBody += "box-shadow: 0px 3px 6px rgba(50, 50, 50, 0.4);'>";
                mailBody += "<div style='padding: 5px;'>";
                mailBody += "	            <table cellpadding='0' cellspacing='0' width='100%' >";
                mailBody += "		            <tr>";
                mailBody += "			            <td style='width: 76px; padding-right: 10px;' ><img src='" + strLogo + "' /></td>";
                mailBody += "			            <td style='width: 48px; padding-left: 10px; border-left:1px solid #999999' ><img src='img/emails/eselect_logo.jpg' /></td>";
                mailBody += "			            <td style='width: 300px;;'></td>";
                mailBody += "			            <td style='width: 35px;' ><a href='http://www.facebook.com/annectosindia'><img src='img/emails/facebook.png' /></a></td>";
                mailBody += "			            <td style='width: 35px;' ><a href='https://twitter.com/annectos'><img src='img/emails/twitter.png' /></a></td>";
                mailBody += "			            <td style='width: 35px;'><a href=http://www.linkedin.com/company/annecto-rewards-&-retail-pvt-ltd-/'><img src='img/emails/linkedin.png' /></a></td>";
                mailBody += "		            </tr>";
                mailBody += "		            </table>";
                mailBody += "		            </div>";
                mailBody += "		            <div style='background: #f5c405; line-height:15px;  font-style: italic; font-size:12px; padding: 5px;'>";
                mailBody += "			            <table cellpadding='0' cellspacing='0'  >";
                mailBody += "		            <tr>";
                mailBody += "		            <td> <img src='img/emails/shiped.png' /></td>";
                mailBody += "		            <td style='font-size: 12px; font-style:italic; padding-left: 10px;'></td>";
                mailBody += "		            </tr>";
                mailBody += "		            </table>";
                mailBody += "		            </div>";
                mailBody += "            </div>";
                mailBody += "        </td>";
                mailBody += "    </tr>";
                mailBody += "    <tr>";
                mailBody += "        <td style='background: #fff; padding: 10px; font-size: 13px'>";
                mailBody += "        Hi " + ShippingName + ", <br /><br />";
                mailBody += "";
                mailBody += "Thank you for shopping at " + strCompanyUrl + "<br /><br />";
                mailBody += "This email contains your order summary. When the item(s) in your order are shipped, you will receive an email with the Courier Tracking ID and the link where you can track your order. You can also check the status of your order by <a style='color: #41a0ff; text-decoration:none' href='" + strMyorderURL + "'>clicking here</a>.<br /><br />";
                mailBody += "Please find below the summary of your order";
                mailBody += "        </td>";
                mailBody += "        </tr>";
                mailBody += "        <tr>";
                mailBody += "        <td style='background: #fff; padding: 0px 10px;  font-size: 16px;color:#3b3a3a; font-weight:bold '>";
                mailBody += "        <div style='border-top:1px dotted #cccccc; border-bottom:1px dotted #cccccc; padding-top: 5px; padding-bottom: 5px;'>";
                mailBody += "        Order No. #" + OrderID;
                mailBody += "        </div>";
                mailBody += "        </td>";
                mailBody += "        </tr>";
                mailBody += "    <tr>";
                mailBody += "        <td style='background: #fff; padding: 10px; font-size: 13px'>";
                mailBody += "        <table cellpadding='0' cellspacing='0' width='100%' >";

                int k = 0;
                //Loop for Product - Start
                foreach (cart_item ord in ordlist)
                {
                    k = k + 1;
                    lineitemprice = ord.quantity * ord.final_offer;

                    mailBody += "            <tr>";
                    mailBody += "             <td colspan='6' style='color: #333333; padding-top:10px; padding-bottom:10px; font-size:13px; font-weight:bold' >" + k.ToString() + " Product Code: " + ord.sku + " - " + ord.name + "</td>	";
                    mailBody += "            </tr>";


                    mailBody += "            <tr>";
                    mailBody += "             <td style='text-align: center; padding-left: 10px; padding-right: 10px;'  ><img src='" + ord.image.thumb_link + "' /></td>	";
                    mailBody += "             <td style='color: #666666; font-size: 13px; padding-left: 10px; padding-right: 10px; text-align: center;'>Size <div style='margin-top: 10px; color: #333333'>#Size</div></td>";
                    mailBody += "             <td style='color: #666666; font-size: 13px;  text-align: center;'><div style=' border-left: 1px solid #ccc; padding-left: 10px; padding-right: 10px;'>Quantity <div style='margin-top: 10px; color: #333333; '>" + ord.quantity + "</div></div></td>";
                    mailBody += "              <td style='color: #666666; font-size: 13px; text-align: center;'><div style=' border-left: 1px solid #ccc; padding-left: 10px; padding-right: 10px;'>Unit Price <div style='margin-top: 10px; color: #333333'>" + ord.mrp + "</div></div></td>";
                    mailBody += "              <td style='color: #666666; font-size: 13px;  text-align: center;'><div style=' border-left: 1px solid #ccc; padding-left: 10px; padding-right: 10px;'>Discount <div style='margin-top: 10px; color: #ff0000'>" + ord.discount + " %" + "</div></div></td> ";    //Diwakar 30/01/2014
                    mailBody += "              <td style='color: #666666; font-size: 13px;  text-align: center;'> <div style=' border-left: 1px solid #ccc; padding-left: 10px; padding-right: 10px;'>Sub Total <div style='margin-top: 10px; color: #333333'>" + lineitemprice + "</div></div></td>";
                    mailBody += "            </tr>";

                    shipmentvalue = shipmentvalue + lineitemprice;
                    discount = discount + Convert.ToDouble(ord.discount);
                    finalamt = shipmentvalue;
                }

                if (finalamt >= Convert.ToDouble(ShippingThreshold))
                {
                    shippingAmount = "Free";
                }
                else
                    finalamt = finalamt + Convert.ToDouble(shippingAmount);

                expshippingAmount = Convert.ToString(totalamount - finalamt);

                //Loop for Product - End 
                /*
                mailBody += "            <tr>";
                mailBody += "             <td colspan='6' style='color: #333333; padding-top:10px; padding-bottom:10px; font-size:13px; font-weight:bold' >2. Product Code: 00328 - MeeMee Baby Carrier (Black)</td>	";
                mailBody += "            </tr>";
            
                mailBody += "            <tr>";
                mailBody += "             <td style='text-align: center; padding-left: 10px; padding-right: 10px;'  ><img src='img/emails/product2.png' /></td>	";
                mailBody += "             <td style='color: #666666; font-size: 13px; padding-left: 10px; padding-right: 10px; text-align: center;'>Size <div style='margin-top: 10px; color: #333333'>-</div></td>";
                mailBody += "             <td style='color: #666666; font-size: 13px;  text-align: center;'><div style=' border-left: 1px solid #ccc; padding-left: 10px; padding-right: 10px;'>Quantity <div style='margin-top: 10px; color: #333333; '>1</div></div></td>";
                mailBody += "              <td style='color: #666666; font-size: 13px; text-align: center;'><div style=' border-left: 1px solid #ccc; padding-left: 10px; padding-right: 10px;'>Unit Price <div style='margin-top: 10px; color: #333333'>1200 </div></div></td>";
                mailBody += "              <td style='color: #666666; font-size: 13px;  text-align: center;'><div style=' border-left: 1px solid #ccc; padding-left: 10px; padding-right: 10px;'>Discount <div style='margin-top: 10px; color: #ff0000'>201</div></div></td>";
                mailBody += "              <td style='color: #666666; font-size: 13px;  text-align: center;'> <div style=' border-left: 1px solid #ccc; padding-left: 10px; padding-right: 10px;'>Sub Total <div style='margin-top: 10px; color: #333333'>999</div></div></td>";
                mailBody += "            </tr>";
                 */
                mailBody += "            <tr>";
                mailBody += "	            <td style='text-align: right; padding:10px 0px;' colspan='5' >";
                mailBody += "	            <div style='border-top: 1px dotted #ccc; width:100%; float:left;  padding:5px 0px '>";
                mailBody += "	            <div style='float: right; text-align:left'>";
                mailBody += "		            <div style='font-weight: bold; padding-right:20px; font-size:12px; color:#333333; '>Shipment Value </div>";
                //mailBody += "		            <div style='font-weight: bold; padding-right:20px; font-size:12px; color:#333333; '>Discount   </div> ";
                mailBody += "		            <div style='font-weight: bold; padding-right:20px; font-size:12px; color:#333333; '>Shipping   </div>";
                mailBody += "		            <div style='font-weight: bold; padding-right:20px; font-size:12px; color:#333333; '>Express Shipping   </div>";
                mailBody += "		            <div style='font-weight: bold; padding-right:20px; font-size:12px; color:#333333; '>Points Redeemed   </div>";
                mailBody += "		            <div style='font-weight: bold; padding-right:20px; font-size:12px; color:#333333; '>Gift Voucher    </div>";
                mailBody += "		            <div style='font-weight: bold; padding-right:20px; font-size:12px; color:#333333; '>Promo Code    </div>";
                mailBody += "		            <div style='font-weight: bold; padding-right:20px; font-size:12px; color:#333333; '>Cash/Card Paid  </div>";
                mailBody += "		            </div>";
                mailBody += "	            </div>";
                mailBody += "	            </td>";
                mailBody += "	            <td style='text-align: right'>";
                mailBody += "	            <div style='border-top: 1px dotted #ccc; padding:5px 0px '>";
                mailBody += "	            <div style='font-weight: bold; padding-right:20px; font-size:12px; color:#333333; '>" + shipmentvalue.ToString() + "</div>";
                //mailBody += "	            <div style='font-weight: bold; padding-right:20px; font-size:12px; color:#ff0000; '>" + discount.ToString() + "</div>";
                mailBody += "	            <div style='font-weight: bold; padding-right:20px; font-size:12px; color:#038203; '>" + shippingAmount.ToString() + "</div>";
                mailBody += "	            <div style='font-weight: bold; padding-right:20px; font-size:12px; color:#038203; '>" + expshippingAmount.ToString() + "</div>";
                mailBody += "	            <div style='font-weight: bold; padding-right:20px; font-size:12px; color:#038203; '>" + points.ToString() + "</div>";
                mailBody += "	            <div style='font-weight: bold; padding-right:20px; font-size:12px; color:#038203; '>" + voucheramt.ToString() + "</div>";
                mailBody += "	            <div style='font-weight: bold; padding-right:20px; font-size:12px; color:#038203; '>" + PromoOffer.ToString() + "</div>";
                mailBody += "	            <div style='font-weight: bold; padding-right:20px; font-size:12px; color:#038203; '>" + paidamt.ToString() + "</div>";
                mailBody += "	            </div>";
                mailBody += "	            </td>";
                mailBody += "            </tr>";
                mailBody += "            <tr>";
                mailBody += "	            <td valign='top' colspan='5' >";
                mailBody += "	            <div style='float: right; text-align:left; border-top: 1px dotted #ccc; width: 110px; padding-left: 40px; padding-top:5px;'>";
                mailBody += "	            <div style='font-weight: bold; padding-right:20px; font-size:12px; color:#333333; '>Amount paid   </div>";
                mailBody += "	            </div>";
                mailBody += "	            </td>";
                mailBody += "	            <td  valign='top'  style='text-align: right'>";
                mailBody += "	            <div style='border-top: 1px dotted #ccc; padding:5px 0px '>";
                mailBody += "	            <div style='font-weight: bold; padding-right:20px; font-size:12px; color:#333333; '>" + totalamount.ToString() + "</div>"; //finalamt.ToString()
                mailBody += "	            </div>";
                mailBody += "	            </td>";
                mailBody += "            </tr>";
                mailBody += "            <tr>";
                mailBody += "	            <td valign='top' style='font-size: 12px; padding-top:20px; color: #333333' colspan='6' >";
                mailBody += "	            <b>Shipping Address:</b>    <br />";
                mailBody += ShippingName + " <br />    ";
                mailBody += ShippingAddress + ",  <br />    ";
                mailBody += ShippingCity + " <br />    ";
                mailBody += ShippingState + " - " + ShippingPin + " <br />";
                mailBody += "Mobile: " + strMobileNo + " <br /> <br />";
                //mailBody += "You can track or manage your order at any time by going to <a href='#' style='color: #41a0ff; text-decoration:none'>http://www.annectos.in/myaccounts.php?view=myorders</a>. If you need any assistance or have any questions, feel free to contact us by using the web form at <a href='#' style='color: #41a0ff; text-decoration:none'>http://www.annectos.in/contactus</a> or call us at +91 9686202046 | +91 9972334590";
                mailBody += "If you need any assistance or have any questions, feel free to contact us by using the web form at <a href='" + strCompanyUrl + "index.html#/contact_us' style='color: #41a0ff; text-decoration:none'>" + strCompanyUrl + "index.html#/contact_us" + "</a> or call us at +91 9686202046 | +91 9972334590";
                mailBody += "	            </td>";
                mailBody += "	            </tr>";
                mailBody += "        </table>";
                mailBody += "        </td>";
                mailBody += "        </tr>";
                mailBody += "<tr>";
                mailBody += "        <td style='background: #fff; padding: 10px; font-size: 13px'>";

                //Promotion/Marketting
                /*
                mailBody += "        <div style='font-size: 18px; font-weight:bold; color:#333333; text-transform:uppercase; line-height:30px; border-bottom: 1px solid #ccc'>You may also consider</div>";
                mailBody += "        <div style='padding-top: 10px'>";
                mailBody += "            <table cellspacing='0' cellpadding='5' style='width: 100%'>";
                mailBody += "	            <tr>";
                mailBody += "		            <td align='left' style='width: 33.3%; padding:0px 10px'>";
                mailBody += "			            <div><img src='img/emails/offer1.png' /></div>";
                mailBody += "		            </td>";
                mailBody += "		            <td align='left' style='width: 33.3%; padding:0px 10px'>";
                mailBody += "			            <div><img src='img/emails/offer2.png' /></div>";
                mailBody += "		            </td>";
                mailBody += "		            <td align='left' style='width: 33.3%; padding:0px 10px'>";
                mailBody += "			            <div><img src='img/emails/offer3.png' /></div>";
                mailBody += "		            </td>";
                mailBody += "	            </tr>";
                mailBody += "	            <tr>";
                mailBody += "		            <td align='left' style='width: 33.3%'>";
                mailBody += "			            <div style='font-size: 13px; color:#333; font-weight: bold; padding-left: 10px;'>Rooptex Kurthi</div>";
                mailBody += "		            </td>";
                mailBody += "		            <td align='left' style='width: 33.3%'>";
                mailBody += "			            <div style='font-size: 13px; color:#333; font-weight: bold; padding-left: 10px;'>Grass Black Crew Neck ";
                mailBody += "Sporty T Shirt</div>";
                mailBody += "		            </td>";
                mailBody += "		            <td align='left' style='width: 33.3%'>";
                mailBody += "			            <div style='font-size: 13px; color:#333; font-weight: bold; padding-left: 10px;'>US Polo Assn Blue Shirt</div>";
                mailBody += "		            </td>";
                mailBody += "	            </tr>";
                mailBody += "	            <tr>";
                mailBody += "		            <td  align='left' style='width: 33.3%; '>";
                mailBody += "			            <div style='font-size: 12px; color:#333; border-top:1px dotted #ccc;  padding:5px 10px;'>";
                mailBody += "			            <div><span style='color:#666666'>MRP: <span style='text-decoration: line-through'>1790</span></span> | 60% off </div>";
                mailBody += "			            <div style='color: #ff6600'>Offer: 758 <span style='color: #333'>|</span> Points: 758 </div>";
                mailBody += "			            </div>";
                mailBody += "		            </td>";
                mailBody += "		            <td  align='left' style='width: 33.3%; '>";
                mailBody += "			            <div style='font-size: 12px; color:#333; border-top:1px dotted #ccc;  padding:5px 10px;'>";
                mailBody += "			            <div><span style='color:#666666'>MRP: <span style='text-decoration: line-through'>1790</span></span> | 60% off </div>";
                mailBody += "			            <div style='color: #ff6600'>Offer: 758 <span style='color: #333'>|</span> Points: 758 </div>";
                mailBody += "			            </div>";
                mailBody += "		            </td>";
                mailBody += "		            <td  align='left' style='width: 33.3%; '>";
                mailBody += "			            <div style='font-size: 12px; color:#333; border-top:1px dotted #ccc;  padding:5px 10px;'>";
                mailBody += "			            <div><span style='color:#666666'>MRP: <span style='text-decoration: line-through'>1790</span></span> | 60% off </div>";
                mailBody += "			            <div style='color: #ff6600'>Offer: 758 <span style='color: #333'>|</span> Points: 758 </div>";
                mailBody += "			            </div>";
                mailBody += "		            </td>";
                mailBody += "	            </tr>";
                mailBody += "            </table>";
                mailBody += "        </div>";
                 */
                mailBody += "        </td>";
                mailBody += "</tr>";
                /*mailBody += "<tr>";
                mailBody += "        <td style='background: #fff; padding: 10px; font-size: 13px'>";
                mailBody += "        <table style='width: 100%' cellpadding='0' cellspacing='0'>";
                mailBody += "            <tr>";
                mailBody += "	            <td align='left'><a href='#'><img src='img/emails/add1.png' /></a></td>";
                mailBody += "	            <td align='right'><a href='#'><img src='img/emails/add2.png' /></a></td>";
                mailBody += "            </tr>";
                mailBody += "        </table>";
                mailBody += "        </td>";
                mailBody += "        </tr>";*/
                mailBody += "</table>";
                /*mailBody += "<table cellpadding='0' cellspacing='0' width='500' align='center'  style=' font-size: 12px; color: #666666'>";
                mailBody += "    <tr>";
                mailBody += "        <td style='padding-top: 10px;' align='center'>customerfirst@annectos.in   call us at +91 9686202046 | +91 9972334590";
                mailBody += "</td>";
                mailBody += "    </tr>";
                mailBody += "    <tr>";
                mailBody += "        <td style='padding-top: 10px; vertical-align: top;' align='center'>Connect With Us <a href='#'><img width='18' height='18' src='img/emails/facebook.png' /></a>  <a href='#'><img width='18' height='18' src='img/emails/twitter.png' /></a>  <a href='#'><img width='18' height='18' src='img/emails/linkedin.png' /></a>";
                mailBody += "</td>";
                mailBody += "    </tr>";
                mailBody += "    <tr>";
                mailBody += "        <td style='padding-top: 10px; font-size: 10px;' align='center'>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. All Rights Reserved. www.annectos.in";
                mailBody += "</td>";
                mailBody += "    </tr>";
                mailBody += "</table>";*/
                mailBody += "</body>";
                mailBody += "</html>";


                mailBody = mailBody.Replace("img/emails/", ConfigurationSettings.AppSettings["email_img"].ToString() + "/img/emails/");
                return mailBody;
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return "";
            }
        }

        private Boolean UpdateDiscountCode(string email_id, string discount_code)
        {
            DataAccess da = new DataAccess();
            try
            {
                if (email_id.Trim().Length > 0 && discount_code.Trim().Length > 0)
                {
                    //int i = da.ExecuteSP("user_discount_code_update",
                    //    da.Parameter("_user_id", email_id),
                    //    da.Parameter("_discount_code", discount_code));

                    try
                    {
                        mongo_user_discount_code omudc = new mongo_user_discount_code();
                        store_repository osr = new store_repository();
                        var next_user_discount_code_id = osr.getNextSequence("user_discount_code_id", "seq");
                        omudc._id = ObjectId.GenerateNewId().ToString();
                        omudc.id = convertToDouble(next_user_discount_code_id.seq);
                        omudc.user_id = convertToString(email_id);
                        omudc.discount_code = convertToString(discount_code);
                        dal.DataAccess dal = new DataAccess();
                        BsonDocument bd_cat = omudc.ToBsonDocument();
                        string result = dal.mongo_write("user_discount_code", bd_cat);

                    }
                    catch (Exception ex)
                    {
                        ExternalLogger.LogError(ex, this, "#");
                        return false;
                    }

                }

                return true;
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return false;
            }
        }

        //public DataTable GetDataFromUserID(Int64 OrderID)
        //{
        //    try
        //    {
        //        DataAccess da = new DataAccess();
        //        DataTable dt = da.ExecuteDataTable("GetShippingDetail",
        //            da.Parameter("_orderid", OrderID.ToString()));

        //        return dt;
        //    }
        //    catch (Exception ex)
        //    {
        //        ExternalLogger.LogError(ex, this, "#");
        //        return new DataTable();
        //    }
        //}


        public mongo_shipping_address GetDataFromUserID(Int64 OrderID)
        {
            try
            {
                mongo_shipping_address oshipping_address = new mongo_shipping_address();

                // evoke mongodb connection method in dal
                dal.DataAccess dal = new DataAccess();
                string q = "{order_id:" + OrderID + "}";
                BsonDocument document = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>(q);
                QueryDocument queryDoc = new QueryDocument(document);
                string[] include_fields = new string[] { "order_id", "shipping_address" };
                string[] exclude_fields = new string[] { "_id" };
                MongoCursor cursor = dal.execute_mongo_db("order", queryDoc, include_fields, exclude_fields);

                foreach (var c in cursor)
                {

                    oshipping_address = check_field(c.ToBsonDocument(), "shipping_address") ? JsonConvert.DeserializeObject<mongo_shipping_address>(c.ToBsonDocument()["shipping_address"].ToString()) : null;

                }

                return oshipping_address;

            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return new mongo_shipping_address();
            }
        }

        //public DataTable GetCompanyInfo(string storename)
        //{
        //    try
        //    {
        //        DataAccess da = new DataAccess();
        //        DataTable dt = da.ExecuteDataTable("get_company_info",
        //            da.Parameter("cname", storename));
        //        return dt;
        //    }
        //    catch (Exception ex)
        //    {
        //        ExternalLogger.LogError(ex, this, "#");
        //        return new DataTable();
        //    }
        //}

        //public DataTable GetDataFromOrder(Int64 OrderID)
        //{
        //    try
        //    {
        //        DataAccess da = new DataAccess();
        //        DataTable dt = da.ExecuteDataTable("get_order_info",
        //                 da.Parameter("_orderid", OrderID)
        //                );
        //        return dt;
        //    }
        //    catch (Exception ex)
        //    {
        //        ExternalLogger.LogError(ex, this, "#");
        //        return new DataTable();
        //    }
        //}


        public List<mongo_order> GetDataFromOrder(Int64 OrderID)
        {
            List<mongo_order> olmo = new List<mongo_order>();

            try
            {
                // evoke mongodb connection method in dal
                dal.DataAccess dal = new DataAccess();
                BsonDocument document = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>("{order_id:" + OrderID + "}");
                QueryDocument queryDoc = new QueryDocument(document);
                MongoCursor cursor = dal.execute_mongo_db("order", queryDoc);


                foreach (var c in cursor)
                {
                    mongo_order omo = new mongo_order();
                    omo.order_id = check_field(c.ToBsonDocument(), "order_id") ? convertToDouble(c.ToBsonDocument()["order_id"]) : 0;
                    omo.cart_id = check_field(c.ToBsonDocument(), "cart_id") ? convertToString(c.ToBsonDocument()["cart_id"]) : null;
                    //  omo.cart_data = check_field(c.ToBsonDocument(), "cart_data") ? JsonConvert.DeserializeObject<mongo_cart_data>(c.ToBsonDocument()["cart_data"].ToString()) : null;
                    omo.user_id = check_field(c.ToBsonDocument(), "user_id") ? convertToString(c.ToBsonDocument()["user_id"]) : null;
                    //   omo.user_data = check_field(c.ToBsonDocument(), "user_data") ? JsonConvert.DeserializeObject<mongo_user_data>(c.ToBsonDocument()["user_data"].ToString()) : null;
                    omo.discount_code = check_field(c.ToBsonDocument(), "discount_code") ? convertToString(c.ToBsonDocument()["discount_code"]) : null;
                    omo.total_amount = check_field(c.ToBsonDocument(), "total_amount") ? convertToDouble(c.ToBsonDocument()["total_amount"]) : 0;
                    omo.points = check_field(c.ToBsonDocument(), "points") ? convertToDouble(c.ToBsonDocument()["points"]) : 0;
                    omo.paid_amount = check_field(c.ToBsonDocument(), "paid_amount") ? convertToDouble(c.ToBsonDocument()["paid_amount"]) : 0;
                    omo.status = check_field(c.ToBsonDocument(), "status") ? convertToInt(c.ToBsonDocument()["status"]) : 0;
                    omo.payment_info = check_field(c.ToBsonDocument(), "payment_info") ? convertToString(c.ToBsonDocument()["payment_info"]) : null;
                    omo.rejection_reason = check_field(c.ToBsonDocument(), "rejection_reason") ? convertToString(c.ToBsonDocument()["rejection_reason"]) : null;
                    //omo.create_ts = check_field(c.ToBsonDocument(), "create_ts") ? convertToDate(c.ToBsonDocument()["create_ts"]) : System;
                    //omo.modify_ts = check_field(c.ToBsonDocument(), "id") ? convertToString(c.ToBsonDocument()["id"]) : null;
                    //omo.pay_sent_ts = check_field(c.ToBsonDocument(), "id") ? convertToString(c.ToBsonDocument()["id"]) : null;
                    //omo.pay_conf_ts = check_field(c.ToBsonDocument(), "id") ? convertToString(c.ToBsonDocument()["id"]) : null;
                    omo.order_display_id = check_field(c.ToBsonDocument(), "order_display_id") ? convertToString(c.ToBsonDocument()["order_display_id"]) : null;
                    omo.store = check_field(c.ToBsonDocument(), "store") ? convertToString(c.ToBsonDocument()["store"]) : null;
                    omo.shipping_info_id = check_field(c.ToBsonDocument(), "shipping_info_id") ? convertToDouble(c.ToBsonDocument()["shipping_info_id"]) : 0;
                    omo.shipping_address = check_field(c.ToBsonDocument(), "shipping_address") ? JsonConvert.DeserializeObject<mongo_shipping_address>(c.ToBsonDocument()["shipping_address"].ToString()) : null;
                    omo.points_amount = check_field(c.ToBsonDocument(), "points_amount") ? convertToDouble(c.ToBsonDocument()["points_amount"]) : 0;
                    omo.egift_vou_no = check_field(c.ToBsonDocument(), "egift_vou_no") ? convertToString(c.ToBsonDocument()["egift_vou_no"]) : null;
                    omo.egift_vou_amt = check_field(c.ToBsonDocument(), "egift_vou_amt") ? convertToDouble(c.ToBsonDocument()["egift_vou_amt"]) : 0;
                    omo.shipping_info = check_field(c.ToBsonDocument(), "shipping_info") ? convertToString(c.ToBsonDocument()["shipping_info"]) : null;
                    omo.delayed_note = check_field(c.ToBsonDocument(), "delayed_note") ? convertToString(c.ToBsonDocument()["delayed_note"]) : null;
                    omo.cancellation_note = check_field(c.ToBsonDocument(), "cancellation_note") ? convertToString(c.ToBsonDocument()["cancellation_note"]) : null;
                    omo.courier_track_no = check_field(c.ToBsonDocument(), "courier_track_no") ? convertToString(c.ToBsonDocument()["courier_track_no"]) : null;
                    omo.courier_track_link = check_field(c.ToBsonDocument(), "courier_track_link") ? convertToString(c.ToBsonDocument()["courier_track_link"]) : null;
                    omo.discount_amount = check_field(c.ToBsonDocument(), "discount_amount") ? convertToDouble(c.ToBsonDocument()["discount_amount"]) : 0;
                    //omo.dispatch_date = check_field(c.ToBsonDocument(), "id") ? convertToString(c.ToBsonDocument()["id"]) : null;
                    omo.courier_cost = check_field(c.ToBsonDocument(), "courier_cost") ? convertToDouble(c.ToBsonDocument()["courier_cost"]) : 0;
                    omo.shipping_amount = check_field(c.ToBsonDocument(), "shipping_amount") ? convertToDouble(c.ToBsonDocument()["shipping_amount"]) : 0;
                    //omo.expiry_datetime = check_field(c.ToBsonDocument(), "expiry_datetime") ? convertToDate(c.ToBsonDocument()["expiry_datetime"]) : null;
                    omo.extended_reason = check_field(c.ToBsonDocument(), "extended_reason") ? convertToString(c.ToBsonDocument()["extended_reason"]) : null;
                    olmo.Add(omo);

                }
                return olmo;
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                throw;
            }
        }


        //public DataTable GetDiscountCodeInfo(string disc_code)
        //{
        //    try
        //    {
        //        DataAccess da = new DataAccess();
        //        //DataTable dt = da.ExecuteDataTable("get_cart_data",
        //        DataTable dt = da.ExecuteDataTable("get_disc_code_info",
        //                 da.Parameter("_discount_code", disc_code)
        //                );
        //        return dt;
        //    }
        //    catch (Exception ex)
        //    {
        //        ExternalLogger.LogError(ex, this, "#");
        //        return new DataTable();
        //    }
        //}


        public List<mongo_discount_codes> GetDiscountCodeInfo(string disc_code)
        {

            List<mongo_discount_codes> olmdc = new List<mongo_discount_codes>();
            try
            {

                dal.DataAccess dal = new DataAccess();
                string q = "{discount_code:'" + disc_code + "'}}";
                BsonDocument document = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>(q);
                QueryDocument queryDoc = new QueryDocument(document);
                MongoCursor cursor = dal.execute_mongo_db("discount_codes", queryDoc);

                foreach (var c in cursor)
                {
                    mongo_discount_codes omdc = new mongo_discount_codes();
                    omdc.discount_code = check_field(c.ToBsonDocument(), "discount_code") ? convertToString(c.ToBsonDocument()["discount_code"]) : null;
                    omdc.content_url = check_field(c.ToBsonDocument(), "content_url") ? convertToString(c.ToBsonDocument()["content_url"]) : null;
                    omdc.rule = check_field(c.ToBsonDocument(), "courier_track_no") ? JsonConvert.DeserializeObject<mongo_discount_rule>(c.ToBsonDocument()["user_data"].ToString()) : null;
                    //omdc.start_date = check_field(c.ToBsonDocument(), "start_date") ? convertToString(c.ToBsonDocument()["start_date"]) : null;
                    //omdc.end_date = check_field(c.ToBsonDocument(), "courier_track_no") ? convertToString(c.ToBsonDocument()["courier_track_no"]) : null;
                    //omdc.create_ts = check_field(c.ToBsonDocument(), "courier_track_no") ? convertToString(c.ToBsonDocument()["courier_track_no"]) : null;
                    //omdc.modify_ts = check_field(c.ToBsonDocument(), "courier_track_no") ? convertToString(c.ToBsonDocument()["courier_track_no"]) : null;
                    omdc.create_by = check_field(c.ToBsonDocument(), "create_by") ? convertToString(c.ToBsonDocument()["create_by"]) : null;
                    omdc.modify_by = check_field(c.ToBsonDocument(), "modify_by") ? convertToString(c.ToBsonDocument()["modify_by"]) : null;
                    omdc.store = check_field(c.ToBsonDocument(), "store") ? convertToString(c.ToBsonDocument()["store"]) : null;
                    olmdc.Add(omdc);

                }


            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return new List<mongo_discount_codes>();
            }

            return olmdc;
        }



        //public DataTable GetUserPointBal(Int64 OrderID)
        //{
        //    try
        //    {
        //        DataAccess da = new DataAccess();
        //        DataTable dt = da.ExecuteDataTable("get_user_point_bal",
        //                 da.Parameter("_order_id", OrderID)
        //                );
        //        return dt;
        //    }
        //    catch (Exception ex)
        //    {
        //        ExternalLogger.LogError(ex, this, "#");
        //        return new DataTable();
        //    }
        //}



        public double GetUserPointBal(string user_id, string store)
        {

            double total_credit_point = 0;
            double total_debit_point = 0;
            double point_balance = 0;

            try
            {
                // evoke mongodb connection method in dal
                dal.DataAccess dal = new DataAccess();

                var match = new BsonDocument 
                { 
                    { 
                        "$match", 
                        new BsonDocument 
                            { 
                                 {"txn_status", 1},
                                 {"store", store},
                                 {"user_id" ,user_id }                               
                            } 
                    } 
                };

                var group = new BsonDocument 
                { 
                    { "$group", 
                        new BsonDocument 
                            { 
                                { "_id", new BsonDocument 
                                             { 
                                                 { "txn_type","$txn_type" } 
                                             } 
                                }, 
                                { 
                                    "total_point", new BsonDocument 
                                                 { 
                                                     { "$sum", "$txn_amount" } 
                                                 } 
                                },      
                                { 
                                    "Count", new BsonDocument 
                                                 { 
                                                     { "$sum", "$Count" } 
                                                 } 
                                } 
                            } 
                  } 
                };

                var project = new BsonDocument 
                { 
                    { 
                        "$project", 
                        new BsonDocument 
                            { 
                                {"_id", 0}, 
                                {"txn_type", "$_id.txn_type"},
                                {"total_point", "$total_point"},
                                {"Count", 1}, 
                            } 
                    } 
                };

                var pipeline = new[] { match, group, project };
                //string q = "{$match: { $and : [{txn_status: 1},{store: '" + store + "'},{user_id : '" + user_id + "'}]}},{$group: { _id: {'txn_type' : '$txn_type'},total_point: {$sum: \"$txn_amount\"},total_count : {$sum : 1}}},{  $project: {_id:0,  txn_type: '$_id.txn_type',total_point: '$total_point', total_count: '$total_count'}},{$sort: {total_point:-1}}"; 
                //BsonDocument document = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>(q);
                //QueryDocument queryDoc = new QueryDocument(document);
                AggregateResult aggresult = dal.execute_aggregate_mongo_db("user_points", pipeline);

                var user_point = aggresult.Response.Elements.ToArray()[0].Value.AsBsonArray;


                for (int i = 0; i < user_point.Count; i++)
                {

                    mongo_point_output ompo = new mongo_point_output();
                    ompo = JsonConvert.DeserializeObject<mongo_point_output>(user_point[i].ToString());

                    if (ompo.txn_type == "c")
                    {
                        total_credit_point = convertToDouble(ompo.total_point);
                    }
                    if (ompo.txn_type == "d")
                    {
                        total_debit_point = convertToDouble(ompo.total_point);
                    }

                }

                point_balance = total_credit_point - total_debit_point;



            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                throw;
            }


            return point_balance;
        }

        public Double getOrderAmount(Int64 OrderID)
        {
            try
            {
                double ordamt = 0;
                List<mongo_order> dt = GetDataFromOrder(OrderID);
                if (dt != null && dt.Count > 0)
                {
                    ordamt = convertToDouble(dt[0].total_amount);
                }
                return ordamt;
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return 0;
            }
        }

        //public Double getUserPointBalance(Int64 OrderID)
        //{
        //    double point_bal = 0;
        //    try
        //    {
        //        DataTable dt = GetUserPointBal(OrderID);
        //        if (dt != null && dt.Rows.Count > 0)
        //        {
        //            point_bal = Convert.ToDouble(dt.Rows[0]["point_balance"].ToString());
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        ExternalLogger.LogError(ex, this, "#");
        //    }
        //    return point_bal;

        //}


        public Double getUserPointBalance(Int64 OrderID)
        {
            double point_bal = 0;
            try
            {
                var order_details = GetDataFromOrder(OrderID);
                //DataTable dt = GetUserPointBal(OrderID);
                //if (dt != null && dt.Rows.Count > 0)
                //{
                //    point_bal = Convert.ToDouble(dt.Rows[0]["point_balance"].ToString());
                //}

                point_bal = GetUserPointBal(convertToString(order_details[0].user_id), convertToString(order_details[0].store));
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
            }
            return point_bal;

        }


        public int ProcessEmailAndStock(string OrderID, int Status)
        {

            //annectoś e-select : EXL , Order # 572, confirmed
            try
            {
                string email_id = "";
                string strEmailBody = GetOrderStatusBody(OrderID, Status, ref email_id);

                string strSubject = "annectoś e-select : " + strMailSubCompany + " , Order # " + OrderID + ", confirmed";
                helper_repository hr = new helper_repository();
                registration_repository rr = new registration_repository();
                string bcclist = ConfigurationSettings.AppSettings["Management"].ToString();
                if (email_id.Length > 0 && strEmailBody.Length > 0)
                {
                    hr.SendMail(rr.getUserEMailAddress(email_id), bcclist, strSubject, strEmailBody);
                }

                //Diwakar Clear cart 2/2/2014
                //start
                ecomm.model.repository.store_repository sr = new ecomm.model.repository.store_repository();

                if (sr.cart_exists(email_id))
                {
                    sr.del_cart(email_id);
                }
                //end
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
            }
            return 1;

        }

        /// <summary>
        /// This is for buying items only with Points and GV
        /// </summary>
        /// <param name="EmailID"></param>
        /// <param name="OrderID"></param>
        /// <param name="PaidAmt"></param>
        /// <param name="Points"></param>
        /// <param name="Points_INR"></param>
        /// <param name="strActualAmt"></param>
        /// <param name="egiftVouNo"></param>
        /// <param name="egiftVouAmt"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        public Boolean PayWithPointsGV(string EmailID, string OrderID, string PaidAmt, string Points, string Points_INR, string strActualAmt, string egiftVouNo, string egiftVouAmt, string discount_code, string discount_amount, string status)
        {
            try
            {
                DataAccess da = new DataAccess();
                if (string.IsNullOrEmpty(Points))
                    Points = "";
                if (OrderID.Trim().Length > 0)
                {
                    if (PaidAmt.Trim().Length == 0)
                        PaidAmt = "0";
                    if (Points.Trim().Length == 0)
                        Points = "0";
                    if (Points_INR.Trim().Length == 0)
                        Points_INR = "0";
                    if (egiftVouNo.Trim().Length == 0)
                        egiftVouNo = "";
                    if (egiftVouAmt.Trim().Length == 0)
                        egiftVouAmt = "0";
                    if (status.Trim().Length == 0)
                        status = "0";

                    //int i = da.ExecuteSP("initiate_order",
                    //        da.Parameter("_usr_id", EmailID)
                    //        , da.Parameter("_order_id", Convert.ToInt64(OrderID))
                    //        , da.Parameter("_paidamt", Convert.ToDecimal(PaidAmt))
                    //        , da.Parameter("_points", Convert.ToInt64(Points))
                    //        , da.Parameter("_points_amt", Convert.ToDecimal(Points_INR))
                    //        , da.Parameter("_egift_vou_no", egiftVouNo)
                    //        , da.Parameter("_egift_vou_amt", Convert.ToDecimal(egiftVouAmt))
                    //        , da.Parameter("_discount_coupon", discount_code)
                    //        , da.Parameter("_discount_amount", Convert.ToDecimal(discount_amount))
                    //        , da.Parameter("_status", Convert.ToInt32(status))
                    //        );



                    dal.DataAccess dal = new DataAccess();
                    mongo_order omo = new mongo_order();
                    omo.order_id = convertToDouble(OrderID);
                    omo.user_id = convertToString(EmailID);

                    BsonDocument bd_prod = omo.ToBsonDocument();

                    var whereclause = "{order_id:" + convertToDouble(OrderID) + "}";
                    MongoDB.Driver.Builders.UpdateBuilder update = new MongoDB.Driver.Builders.UpdateBuilder();

                    update = MongoDB.Driver.Builders.Update.Set("status", convertToInt(status)).Set("points", convertToDouble(Points)).Set("paid_amount", convertToDouble(PaidAmt)).Set("points_amount", convertToDouble(Points_INR)).Set("egift_vou_no", convertToString(egiftVouNo)).Set("egift_vou_amt", convertToDouble(egiftVouAmt)).Set("discount_code", convertToString(discount_code)).Set("discount_amount", convertToDouble(discount_amount));

                    string result = dal.mongo_update("order", whereclause, bd_prod, update);


                    ProcessOrderStatus(OrderID, PaidAmt, "Paid using Points/GV", status);

                    //GV Update
                    string strGV = egiftVouNo;
                    if (strGV.Trim().Length > 0)
                    {
                        string[] strSplitGV = strGV.Split('|');
                        gift_repository gr = new gift_repository();
                        for (int j = 0; j < strSplitGV.Length; j++)
                        {
                            if (strSplitGV[j] != null && strSplitGV[j] != "undefined")
                            {
                                gr.redeem_gv(strSplitGV[j]);
                            }
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return false;
            }
            return true;
        }

        //public Boolean InitiateOrder(string EmailID, string OrderID, string PaidAmt, string Points, string Points_INR, string strActualAmt, string egiftVouNo, string egiftVouAmt, string discount_coupon, string discount_amount, string status)
        //{
        //    DataAccess da = new DataAccess();

        //    try
        //    {
        //        if (OrderID.Trim().Length > 0)
        //        {
        //            if (String.IsNullOrEmpty(PaidAmt))
        //                PaidAmt = "0";
        //            if (Points.Trim().Length == 0)
        //                Points = "0";
        //            if (Points_INR.Trim().Length == 0)
        //                Points_INR = "0";
        //            if (egiftVouNo.Trim().Length == 0)
        //                egiftVouNo = "";
        //            if (egiftVouAmt.Trim().Length == 0)
        //                egiftVouAmt = "0";
        //            if (status.Trim().Length == 0)
        //                status = "0";

        //            if (string.IsNullOrEmpty(discount_coupon))
        //                discount_coupon = "";
        //            if (string.IsNullOrEmpty(discount_amount))
        //                discount_amount = "0";


        //            int i = da.ExecuteSP("initiate_order",
        //                    da.Parameter("_usr_id", EmailID)
        //                    , da.Parameter("_order_id", Convert.ToInt64(OrderID))
        //                    , da.Parameter("_paidamt", Convert.ToDecimal(PaidAmt))
        //                    , da.Parameter("_points", Convert.ToInt64(Points))
        //                    , da.Parameter("_points_amt", Convert.ToDecimal(Points_INR))
        //                    , da.Parameter("_egift_vou_no", egiftVouNo)
        //                    , da.Parameter("_egift_vou_amt", Convert.ToDecimal(egiftVouAmt))
        //                    , da.Parameter("_discount_coupon", discount_coupon)
        //                    , da.Parameter("_discount_amount", Convert.ToDecimal(discount_amount))
        //                    , da.Parameter("_status", Convert.ToInt32(status))
        //                    );
        //        }
        //        return true;
        //    }
        //    catch (Exception ex)
        //    {
        //        ExternalLogger.LogError(ex, this, "#");
        //        return false;
        //    }
        //}

        public Boolean InitiateOrder(string EmailID, string OrderID, string PaidAmt, string Points, string Points_INR, string strActualAmt, string egiftVouNo, string egiftVouAmt, string discount_coupon, string discount_amount, string status)
        {
            DataAccess da = new DataAccess();

            try
            {
                if (OrderID.Trim().Length > 0)
                {
                    if (String.IsNullOrEmpty(PaidAmt))
                        PaidAmt = "0";
                    if (Points.Trim().Length == 0)
                        Points = "0";
                    if (Points_INR.Trim().Length == 0)
                        Points_INR = "0";
                    if (egiftVouNo.Trim().Length == 0)
                        egiftVouNo = "";
                    if (egiftVouAmt.Trim().Length == 0)
                        egiftVouAmt = "0";
                    if (status.Trim().Length == 0)
                        status = "0";

                    if (string.IsNullOrEmpty(discount_coupon))
                        discount_coupon = "";
                    if (string.IsNullOrEmpty(discount_amount))
                        discount_amount = "0";


                    dal.DataAccess dal = new DataAccess();
                    mongo_order omo = new mongo_order();
                    omo.order_id = convertToDouble(OrderID);
                    omo.user_id = convertToString(EmailID);

                    BsonDocument bd_prod = omo.ToBsonDocument();
                    var whereclause = "{order_id:" + convertToDouble(OrderID) + "}";
                    MongoDB.Driver.Builders.UpdateBuilder update = new MongoDB.Driver.Builders.UpdateBuilder();

                    update = MongoDB.Driver.Builders.Update.Set("status", convertToInt(status)).Set("points", convertToDouble(Points)).Set("paid_amount", convertToDouble(PaidAmt)).Set("points_amount", convertToDouble(Points_INR)).Set("egift_vou_no", convertToString(egiftVouNo)).Set("egift_vou_amt", convertToDouble(egiftVouAmt)).Set("discount_code", convertToString(discount_coupon)).Set("discount_amount", convertToDouble(discount_amount));

                    string result = dal.mongo_update("order", whereclause, bd_prod, update);

                }
                return true;
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return false;
            }
        }


        //public string GetCompanyURLFromOrderID(string OrderID)
        //{
        //    string strCompanyUrl = "";
        //    try
        //    {
        //        DataAccess da = new DataAccess();
        //        DataTable dtCompany = da.ExecuteDataTable("get_company_url",
        //            da.Parameter("_orderid", OrderID));

        //        if (dtCompany != null && dtCompany.Rows.Count > 0)
        //        {
        //            //strLogo = dtCompany.Rows[0]["logo"].ToString();
        //            strCompanyUrl = dtCompany.Rows[0]["root_url_1"].ToString();
        //            //strMailSubCompany = dtCompany.Rows[0]["displayname"].ToString();
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        ExternalLogger.LogError(ex, this, "#");
        //    }
        //    return strCompanyUrl;
        //}


        public string GetCompanyURLFromOrderID(string OrderID)
        {
            string strCompanyUrl = "";
            try
            {

                dal.DataAccess dal = new DataAccess();
                string q = "{ order_id:" + OrderID + "}";
                BsonDocument document = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>(q);
                QueryDocument queryDoc = new QueryDocument(document);
                string[] include_fields = new string[] { "order_id", "user_id", "store" };
                string[] exclude_fields = new string[] { "_id" };
                MongoCursor cursor = dal.execute_mongo_db("order", queryDoc, include_fields, exclude_fields);

                string Store = "";

                foreach (var c in cursor)
                {
                    Store = check_field(c.ToBsonDocument(), "store") ? convertToString(c.ToBsonDocument()["store"]) : null;
                }

                string q1 = "{ name:'" + Store + "'}";
                BsonDocument document1 = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>(q1);
                QueryDocument queryDoc1 = new QueryDocument(document1);
                MongoCursor cursor1 = dal.execute_mongo_db("order", queryDoc1);

                foreach (var c1 in cursor1)
                {
                    strCompanyUrl = check_field(c1.ToBsonDocument(), "root_url_1") ? convertToString(c1.ToBsonDocument()["root_url_1"]) : null;
                }

            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
            }
            return strCompanyUrl;
        }

        /// <summary>
        /// Post Action after Payment Gateway
        /// </summary>
        /// <param name="OrderID"></param>
        /// <param name="PaidAmt"></param>
        /// <param name="remarks"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        //public string ProcessOrderStatus(string OrderID, string PaidAmt, string remarks, string status)
        //{
        //    DataAccess da = new DataAccess();
        //    string strPayInfo = "";
        //    string strRejectInfo = "";

        //    try
        //    {
        //        if (OrderID.Trim().Length > 0)
        //        {
        //            if (String.IsNullOrEmpty(PaidAmt))
        //                PaidAmt = "0";
        //            if (status.Trim().Length == 0)
        //                status = "0";

        //            if (status == "2")
        //            {
        //                strPayInfo = remarks;
        //            }
        //            else if (status == "3")
        //            {
        //                strRejectInfo = remarks;
        //            }
        //            else if (status == "4")
        //            {
        //                strPayInfo = remarks;
        //            }
        //            else if (status == "9")
        //            {
        //                strRejectInfo = remarks;
        //            }

        //            int i = da.ExecuteSP("pay_gateway_order",
        //                     da.Parameter("_order_id", Convert.ToInt64(OrderID))
        //                    , da.Parameter("_paidamt", Convert.ToDecimal(PaidAmt))
        //                    , da.Parameter("_paymentinfo", strPayInfo)
        //                    , da.Parameter("_rejectioninfo", strRejectInfo)
        //                    , da.Parameter("_status", Convert.ToInt32(status))
        //                    );

        //            ProcessEmailAndStock(OrderID, 2);
        //            return "Success";
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        ExternalLogger.LogError(ex, this, "#");
        //        return "Error: " + ex.Message + " -- Trace:" + ex.InnerException + " -- " + ex.StackTrace;
        //    }
        //    return "";
        //}



        public string ProcessOrderStatus(string OrderID, string PaidAmt, string remarks, string status)
        {
            DataAccess da = new DataAccess();
            string strPayInfo = "";
            string strRejectInfo = "";

            try
            {
                if (OrderID.Trim().Length > 0)
                {
                    if (String.IsNullOrEmpty(PaidAmt))
                        PaidAmt = "0";
                    if (status.Trim().Length == 0)
                        status = "0";

                    if (status == "2")
                    {
                        strPayInfo = remarks;
                    }
                    else if (status == "3")
                    {
                        strRejectInfo = remarks;
                    }
                    else if (status == "4")
                    {
                        strPayInfo = remarks;
                    }
                    else if (status == "9")
                    {
                        strRejectInfo = remarks;
                    }

                    //int i = da.ExecuteSP("pay_gateway_order",
                    //         da.Parameter("_order_id", Convert.ToInt64(OrderID))
                    //        , da.Parameter("_paidamt", Convert.ToDecimal(PaidAmt))
                    //        , da.Parameter("_paymentinfo", strPayInfo)
                    //        , da.Parameter("_rejectioninfo", strRejectInfo)
                    //        , da.Parameter("_status", Convert.ToInt32(status))
                    //        );


                    dal.DataAccess dal = new DataAccess();

                    mongo_order omo = new mongo_order();
                    omo.order_id = convertToDouble(OrderID);
                    omo.paid_amount = convertToDouble(PaidAmt);

                    BsonDocument bd_prod = omo.ToBsonDocument();
                    var whereclause = "{order_id:" + convertToDouble(OrderID) + "}";
                    MongoDB.Driver.Builders.UpdateBuilder update = new MongoDB.Driver.Builders.UpdateBuilder();

                    update = MongoDB.Driver.Builders.Update.Set("status", convertToInt(status)).Set("payment_info", convertToString(strPayInfo)).Set("rejection_reason", convertToString(strRejectInfo));
                    string result = dal.mongo_update("order", whereclause, bd_prod, update);

                    List<mongo_order> dt = GetDataFromOrder(convertToLong(OrderID));

                    if (convertToDouble(dt[0].points) > 0 && convertToInt(status) == 2)
                    {
                        mongo_user_points omup = new mongo_user_points();
                        try
                        {
                            omup._id = ObjectId.GenerateNewId().ToString();
                            omup.user_id = convertToString(dt[0].user_id);
                            omup.txn_amount = convertToDouble(dt[0].points);
                            omup.txn_type = "d";
                            omup.txn_ts = System.DateTime.Now;
                            omup.txn_comment = "Amount Paid";
                            omup.txn_status = 1;
                            omup.validity = 0;
                            omup.expiry_date = null;
                            omup.program_type = null;
                            omup.store = convertToString(dt[0].store);

                            BsonDocument bd_cat = omup.ToBsonDocument();
                            string result1 = dal.mongo_write("user_points", bd_cat);

                        }
                        catch (Exception ex)
                        {
                            ExternalLogger.LogError(ex, this, "#");
                            return "";
                        }

                    }

                    ProcessEmailAndStock(OrderID, 2);
                    return "Success";
                }
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return "Error: " + ex.Message + " -- Trace:" + ex.InnerException + " -- " + ex.StackTrace;
            }
            return "";
        }

        /*************End of Payment Gateway Pre & Post Actions***********/


    }
}

