﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ecomm.util.entities;
using ecomm.dal;
using System.Data;
using ecomm.util;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Builders;


namespace ecomm.model.repository
{
    public class admin_repository
    {

        private decimal convertToDecimal(object o)
        {
            try
            {
                if (o != null)
                {
                    return decimal.Parse(o.ToString());
                }
                else { return 0; }
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return 0;
            }
        }

        private double convertToDouble(object o)
        {
            try
            {
                if (o != null)
                {
                    return double.Parse(Math.Round(decimal.Parse(o.ToString()), 3).ToString());
                }
                else { return 0; }
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return 0;
            }
        }
        private string convertToString(object o)
        {
            try
            {
                if ((o != null) && (o.ToString() != ""))
                {
                    return o.ToString();
                }
                else { return ""; }
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return "";
            }
        }

        private int convertToInt(object o)
        {
            try
            {
                if (o != null)
                {
                    return Int32.Parse(o.ToString());
                }
                else { return 0; }
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return 0;
            }
        }





        private bool check_field(BsonDocument doc, string field_name)
        {
            if (doc.Contains(field_name))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        #region ===============Company=====================
        //public string CompanyEntry(COMPANY cf)
        //{
        //    DataAccess da = new DataAccess();
        //    List<OutCollection> oc = new List<OutCollection>();
        //    string guid = "";
        //    try
        //    {

        //        int i = da.ExecuteSP("comp_insert", ref oc
        //                        , da.Parameter("strcompid", cf.id)
        //                        , da.Parameter("strname", cf.name)
        //                        , da.Parameter("strdisplayname", cf.displayname)
        //                        , da.Parameter("strcontactname", cf.contactname)
        //                        , da.Parameter("strphonenumber", cf.phonenumber)
        //                        , da.Parameter("straddress", cf.address)
        //                        , da.Parameter("stremailid", cf.emailid)
        //                        , da.Parameter("strlogo", cf.logo)
        //                        , da.Parameter("inrratio",cf.points_ratio)
        //                        , da.Parameter("dpoint_range_min", cf.point_range_min)
        //                        , da.Parameter("dpoint_range_max", cf.point_range_max)
        //                        , da.Parameter("sroot_url_1", cf.root_url_1)
        //                        , da.Parameter("sroot_url_2", cf.root_url_2)
        //                        , da.Parameter("dshipping_charge", cf.shipping_charge)
        //                        , da.Parameter("dmin_order_for_free_shipping", cf.min_order_for_free_shipping)
        //                        , da.Parameter("intsalestracking", cf.sales_tracking)
        //                        , da.Parameter("strtrackcycle", cf.track_cycle)
        //                        , da.Parameter("intvalidity", cf.validity)
        //                        , da.Parameter("_outmsg", String.Empty, true)
        //                        );

        //        if (i == 1)
        //        {
        //            return "Success";
        //        }
        //        else
        //            return "Failed";
        //    }
        //    catch (Exception ex)
        //    {
        //        ExternalLogger.LogError(ex, this, "#");
        //        return "Failed"; ;
        //    }

        //}


        // Dipanjan

        public string CompanyEntry(mongo_company m_comp)
        {

            m_comp._id = ObjectId.GenerateNewId().ToString();
            try
            {

                dal.DataAccess dal = new DataAccess();
                BsonDocument document = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>("{name:'" + convertToString(m_comp.name) + "'}");
                QueryDocument queryDoc = new QueryDocument(document);
                MongoCursor cursor = dal.execute_mongo_db("company", queryDoc);

                var comp = new List<mongo_company>();
                var match = false;

                foreach (var c in cursor)
                {

                    match = true;
                }

                string result = "";

                if (match == false)
                {
                    store_repository osr = new store_repository();
                    var company_next_id = osr.getNextSequence("companyid", "seq");
                    m_comp.id = convertToDouble(company_next_id.seq); ;
                    m_comp.name.ToLower();
                    BsonDocument bd_comp = m_comp.ToBsonDocument();
                    result = dal.mongo_write("company", bd_comp);

                }
                else
                {
                    result = "Company Name Already Exists";

                }
                return result;
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return "";
            }

        }




        public string update_CompanyEntry(mongo_company mc)
        {
            // evoke mongodb connection method in dal
            mc._id = ObjectId.GenerateNewId().ToString();
            try
            {
                dal.DataAccess dal = new DataAccess();
                BsonDocument bd_prod = mc.ToBsonDocument();
                var whereclause = "{name:'" + mc.name + "'}";
                MongoDB.Driver.Builders.UpdateBuilder update = new UpdateBuilder();
                update = MongoDB.Driver.Builders.Update
                    //  .Set("_id",mc._id)
                   .Set("displayname", convertToString(mc.displayname))
                   .Set("contactname", convertToString(mc.contactname))
                   .Set("phonenumber", convertToString(mc.phonenumber))
                   .Set("address", convertToString(mc.address))
                   .Set("emailid", convertToString(mc.emailid))
                   .Set("logo", convertToString(mc.logo))
                   .Set("company_type", convertToString(mc.company_type))
                   .Set("point_range_min", convertToDouble(mc.point_range_min))
                   .Set("point_range_max", convertToDouble(mc.point_range_max))
                   .Set("root_url_1", convertToString(mc.root_url_1))
                   .Set("root_url_2", convertToString(mc.root_url_2))
                   .Set("shipping_charge", convertToDouble(mc.shipping_charge))
                   .Set("min_order_for_free_shipping", convertToDouble(mc.min_order_for_free_shipping))
                   .Set("sales_tracking", convertToDouble(mc.sales_tracking))
                   .Set("track_cycle", convertToString(mc.track_cycle))
                   .Set("validity", convertToDouble(mc.validity))
                   .Set("inr_point_ratio", convertToDouble(mc.inr_point_ratio));

                string result = dal.mongo_update("company", whereclause, bd_prod, update);

                return result;
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return "";
            }
        }


        public List<mongo_company> PopulateCompany()
        {
            try
            {

                DataAccess da = new DataAccess();

                MongoCursor cursor = da.execute_mongo_db("company");

                var company = new List<mongo_company>();

                mongo_company mongo_comp_new = new mongo_company();
                mongo_comp_new.id = 0;
                mongo_comp_new.name = "New Company";
                company.Add(mongo_comp_new);
                foreach (var c in cursor)
                {
                    mongo_company mongo_comp = new mongo_company();
                    mongo_comp.id = check_field(c.ToBsonDocument(), "id") ? convertToDouble(c.ToBsonDocument()["id"]) : 0;
                    mongo_comp.name = check_field(c.ToBsonDocument(), "name") ? convertToString(c.ToBsonDocument()["name"]) : null;
                    company.Add(mongo_comp);
                }

                return company;

            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                throw;
            }


        }


        public List<mongo_company> postCompanyList()
        {
            try
            {

                DataAccess da = new DataAccess();

                dal.DataAccess dal = new DataAccess();
                // first get the cats that special items belong to
                string q = "{}";
                BsonDocument document = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>(q);
                QueryDocument queryDoc = new QueryDocument(document);
                string[] include_fields = new string[] { "id", "name" };
                string[] exclude_fields = new string[] { "_id" };
                MongoCursor cursor = dal.execute_mongo_db("company", queryDoc, include_fields, exclude_fields);

               // MongoCursor cursor = da.execute_mongo_db("company");

                var company = new List<mongo_company>();
                foreach (var c in cursor)
                {
                    mongo_company mongo_comp = new mongo_company();
                    mongo_comp.id = check_field(c.ToBsonDocument(), "id") ? convertToDouble(c.ToBsonDocument()["id"]) : 0;
                    mongo_comp.name = check_field(c.ToBsonDocument(), "name") ? convertToString(c.ToBsonDocument()["name"]) : null;
                    company.Add(mongo_comp);
                }

                return company;

            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                throw;
            }


        }


        public List<mongo_company> get_company_details(string store)
        {

            List<mongo_company> olmc = new List<mongo_company>();
            try
            {
                // evoke mongodb connection method in dal
                dal.DataAccess dal = new DataAccess();
                string q = "{'name':'" + convertToString(store) + "'}}";
                BsonDocument document = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>(q);
                QueryDocument queryDoc = new QueryDocument(document);
                MongoCursor cursor = dal.execute_mongo_db("company", queryDoc);

                foreach (var c in cursor)
                {
                    mongo_company omc = new mongo_company();

                    omc.id = check_field(c.ToBsonDocument(), "id") ? convertToDouble(c.ToBsonDocument()["id"]) : 0;
                    omc.name = check_field(c.ToBsonDocument(), "name") ? convertToString(c.ToBsonDocument()["name"]) : null;
                    omc.displayname = check_field(c.ToBsonDocument(), "displayname") ? convertToString(c.ToBsonDocument()["displayname"]) : null;
                    omc.contactname = check_field(c.ToBsonDocument(), "contactname") ? convertToString(c.ToBsonDocument()["contactname"]) : null;
                    omc.phonenumber = check_field(c.ToBsonDocument(), "phonenumber") ? convertToString(c.ToBsonDocument()["phonenumber"]) : null;
                    omc.address = check_field(c.ToBsonDocument(), "address") ? convertToString(c.ToBsonDocument()["address"]) : null;
                    omc.emailid = check_field(c.ToBsonDocument(), "emailid") ? convertToString(c.ToBsonDocument()["emailid"]) : null;
                    omc.logo = check_field(c.ToBsonDocument(), "logo") ? convertToString(c.ToBsonDocument()["logo"]) : null;
                    omc.company_type = check_field(c.ToBsonDocument(), "company_type") ? convertToString(c.ToBsonDocument()["company_type"]) : null;
                    omc.point_range_max = check_field(c.ToBsonDocument(), "point_range_max") ? convertToDouble(c.ToBsonDocument()["point_range_max"]) : 0;
                    omc.point_range_min = check_field(c.ToBsonDocument(), "point_range_min") ? convertToDouble(c.ToBsonDocument()["point_range_min"]) : 0;
                    omc.root_url_1 = check_field(c.ToBsonDocument(), "root_url_1") ? convertToString(c.ToBsonDocument()["root_url_1"]) : null;
                    omc.root_url_2 = check_field(c.ToBsonDocument(), "root_url_2") ? convertToString(c.ToBsonDocument()["root_url_2"]) : null;
                    omc.shipping_charge = check_field(c.ToBsonDocument(), "shipping_charge") ? convertToDouble(c.ToBsonDocument()["shipping_charge"]) : 0;
                    omc.min_order_for_free_shipping = check_field(c.ToBsonDocument(), "min_order_for_free_shipping") ? convertToDouble(c.ToBsonDocument()["min_order_for_free_shipping"]) : 0;
                    omc.sales_tracking = check_field(c.ToBsonDocument(), "sales_tracking") ? convertToDouble(c.ToBsonDocument()["sales_tracking"]) : 0;
                    omc.track_cycle = check_field(c.ToBsonDocument(), "track_cycle") ? convertToString(c.ToBsonDocument()["track_cycle"]) : null;
                    omc.validity = check_field(c.ToBsonDocument(), "validity") ? convertToDouble(c.ToBsonDocument()["validity"]) : 0;
                    omc.inr_point_ratio = check_field(c.ToBsonDocument(), "inr_point_ratio") ? convertToDouble(c.ToBsonDocument()["inr_point_ratio"]) : 0;
                    omc.point_range_max = check_field(c.ToBsonDocument(), "point_range_max") ? convertToDouble(c.ToBsonDocument()["point_range_max"]) : 0;
                    omc.point_range_min = check_field(c.ToBsonDocument(), "point_range_min") ? convertToDouble(c.ToBsonDocument()["point_range_min"]) : 0;
                    olmc.Add(omc);
                }


            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                throw;
            }

            return olmc;

        }


        //

        //public string UserEntry(user_security us)
        //{
        //    DataAccess da = new DataAccess();
        //    List<OutCollection> oc = new List<OutCollection>();

        //    try
        //    {

        //        int i = da.ExecuteSP("user_inser_update", ref oc
        //                        , da.Parameter("_Id", us.Id)
        //                        , da.Parameter("_user_name", us.user_name)
        //                        , da.Parameter("_company_id", us.company_id)
        //                        , da.Parameter("_password", us.password)
        //                        , da.Parameter("_user_type", us.user_type)
        //                        , da.Parameter("_outmsg", String.Empty, true)
        //                        );

        //        if (i == 1)
        //        {
        //            return oc[0].strParamValue.ToString();
        //        }
        //        else
        //            return oc[0].strParamValue.ToString();
        //    }
        //    catch (Exception ex)
        //    {
        //        ExternalLogger.LogError(ex, this, "#");
        //        return "Failed";
        //    }

        //}


        public string UserEntry(mongo_user_security us)
        {

            try
            {
                dal.DataAccess dal = new DataAccess();

                us.user_name.ToLower();

                BsonDocument document = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>("{Id:'" + convertToInt(us.Id) + "'}");
                QueryDocument queryDoc = new QueryDocument(document);
                MongoCursor cursor = dal.execute_mongo_db("user_security", queryDoc);

                //   var comp = new List<mongo_company>();
                var match = false;

                foreach (var c in cursor)
                {

                    match = true;
                }

                string result = "";

                if (match == false)
                {
                    store_repository osr = new store_repository();
                    var user_next_id = osr.getNextSequence("user_security_id", "seq");
                    us.Id = convertToInt(user_next_id.seq);
                    BsonDocument bd_comp = us.ToBsonDocument();
                    result = dal.mongo_write("user_security", bd_comp);

                }
                else
                {



                    //BsonDocument bd_prod = us.ToBsonDocument();
                    //var whereclause = "{ID:'" + us.ID + "'}";
                    //MongoDB.Driver.Builders.UpdateBuilder update = new UpdateBuilder();
                    //update = MongoDB.Driver.Builders.Update
                    //   .Set("user_name", convertToString(us.user_name))
                    //   .Set("company_id", convertToInt(us.company_id))
                    //   .Set("paid_amount", convertToDouble(us.paid_amount))
                    //   .Set("password", convertToString(us.password))
                    //   .Set("user_type", convertToString(us.user_type));

                    //result = dal.mongo_update("user_security", whereclause, bd_prod, update);


                }
                return result;

            }

            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return "Failed";
            }

        }


        public string updateUserEntry(mongo_user_security us)
        {
            try
            {

                dal.DataAccess dal = new DataAccess();
                BsonDocument bd_prod = us.ToBsonDocument();
                var whereclause = "{Id:" + convertToInt(us.Id) + "}";
                MongoDB.Driver.Builders.UpdateBuilder update = new UpdateBuilder();
                update = MongoDB.Driver.Builders.Update
                .Set("user_name", convertToString(us.user_name))
                .Set("company_id", convertToInt(us.company_id))
                    //.Set("paid_amount", convertToDouble(us.paid_amount))
                .Set("password", convertToString(us.password))
                .Set("user_type", convertToString(us.user_type));

                string result = dal.mongo_update("user_security", whereclause, bd_prod, update);

                return result;


                //BsonDocument bd_prod = us.ToBsonDocument();
                //var whereclause = "{ID:'" + us.ID + "'}";
                //MongoDB.Driver.Builders.UpdateBuilder update = new UpdateBuilder();
                //update = MongoDB.Driver.Builders.Update
                //   .Set("user_name", convertToString(us.user_name))
                //   .Set("company_id", convertToInt(us.company_id))
                //   .Set("paid_amount", convertToDouble(us.paid_amount))
                //   .Set("password", convertToString(us.password))
                //   .Set("user_type", convertToString(us.user_type));

                //result = dal.mongo_update("user_security", whereclause, bd_prod, update);



            }

            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return "Failed";
            }

        }

        //public List<COMPANY> PopulateCompany()
        //{

        //    DataAccess da = new DataAccess();

        //    List<COMPANY> lcomp = new List<COMPANY>();
        //    DataTable dtResult = new DataTable();

        //    try
        //    {
        //        dtResult = da.ExecuteDataTable("company_details"
        //                        , da.Parameter("_comp_id", 0)
        //                        , da.Parameter("_entry_mode", "N")
        //                        );

        //        if (dtResult != null && dtResult.Rows.Count > 0)
        //        {
        //            for (int i = 0; i < dtResult.Rows.Count; i++)
        //            {
        //                COMPANY comp = new COMPANY();
        //                comp.id = Convert.ToInt32(dtResult.Rows[i]["COMPANY_ID"].ToString());
        //                comp.name = dtResult.Rows[i]["COMPANY_NAME"].ToString();
        //                lcomp.Add(comp);
        //            }
        //        }



        //    }
        //    catch (Exception ex)
        //    {
        //        ExternalLogger.LogError(ex, this, "#");
        //        throw ex;
        //    }

        //    return lcomp;
        //}

        public List<COMPANY> get_comp_details(COMPANY c)
        {

            DataAccess da = new DataAccess();

            List<COMPANY> lcomp = new List<COMPANY>();
            DataTable dtResult = new DataTable();

            try
            {
                dtResult = da.ExecuteDataTable("company_details"
                                 , da.Parameter("_comp_id", c.id)
                                 , da.Parameter("_entry_mode", "E")
                                );

                if (dtResult != null && dtResult.Rows.Count > 0)
                {
                    for (int i = 0; i < dtResult.Rows.Count; i++)
                    {
                        COMPANY comp = new COMPANY();
                        comp.id = Convert.ToInt32(dtResult.Rows[i]["id"].ToString());
                        comp.name = dtResult.Rows[i]["name"].ToString();
                        comp.displayname = dtResult.Rows[i]["displayname"].ToString();
                        comp.contactname = dtResult.Rows[i]["contactname"].ToString();
                        comp.phonenumber = dtResult.Rows[i]["phonenumber"].ToString();
                        comp.address = dtResult.Rows[i]["address"].ToString();
                        comp.emailid = dtResult.Rows[i]["emailid"].ToString();
                        comp.logo = dtResult.Rows[i]["logo"].ToString();
                        comp.points_ratio = convertToDecimal(dtResult.Rows[i]["inr_point_ratio"].ToString());
                        comp.point_range_min = convertToDecimal(dtResult.Rows[i]["point_range_min"].ToString());
                        comp.point_range_max = convertToDecimal(dtResult.Rows[i]["point_range_max"].ToString());
                        comp.root_url_1 = dtResult.Rows[i]["root_url_1"].ToString();
                        comp.root_url_2 = dtResult.Rows[i]["root_url_2"].ToString();
                        comp.shipping_charge = convertToDecimal(dtResult.Rows[i]["shipping_charge"].ToString());
                        comp.min_order_for_free_shipping = convertToDecimal(dtResult.Rows[i]["min_order_for_free_shipping"].ToString());

                        comp.sales_tracking = Convert.ToInt32(dtResult.Rows[i]["sales_tracking"].ToString());
                        comp.track_cycle = dtResult.Rows[i]["track_cycle"].ToString();
                        comp.validity = Convert.ToInt32(dtResult.Rows[i]["validity"].ToString());

                        lcomp.Add(comp);
                    }
                }



            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                throw ex;
            }

            return lcomp;
        }

        #endregion

        #region=========Category=================
        public string CategoryEntry(CATEGORY cate)
        {
            DataAccess da = new DataAccess();
            List<OutCollection> oc = new List<OutCollection>();

            try
            {

                int i = da.ExecuteSP("category_insert", ref oc
                                , da.Parameter("_CAT_ID", cate.CAT_ID)
                                , da.Parameter("_CAT_NAME", cate.CAT_NAME)
                                , da.Parameter("_CAT_DISPLAY_NAME", cate.CAT_DISPLAY_NAME)
                                , da.Parameter("_CAT_DESCRIPTION", cate.CAT_DESCRIPTION)
                                , da.Parameter("_PARENT_CAT_ID", cate.PARENT_CAT_ID)
                                , da.Parameter("_CAT_IMG_URL", cate.CAT_IMG_URL)
                                , da.Parameter("_ACTION_FLAG", 1)
                                , da.Parameter("_OUTMSG", String.Empty, true)
                                );

                if (i == 1)
                {
                    return oc[0].strParamValue.ToString();
                }
                else
                    return "Failed";
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return "Failed"; ;
            }

        }
        #endregion

        #region=========Product=================
        public string ProductEntry(PRODUCT prod)
        {
            DataAccess da = new DataAccess();
            List<OutCollection> oc = new List<OutCollection>();

            try
            {

                int i = da.ExecuteSP("product_insert", ref oc
                                , da.Parameter("_PRODUCT_ID", prod.PRODUCT_ID)
                                , da.Parameter("_PRODUCT_NAME", prod.PRODUCT_NAME)
                                , da.Parameter("_PRODUCT_DISP_NAME", prod.PRODUCT_DISP_NAME)
                                , da.Parameter("_PRODUCT_DESC", prod.PRODUCT_DESC)
                                , da.Parameter("_MRP", prod.MRP)
                                , da.Parameter("_LIST_PRICE", prod.LIST_PRICE)
                                , da.Parameter("_MIN_SALE_PRICE", prod.MIN_SALE_PRICE)
                                , da.Parameter("_PRODUCT_IMG_URL", prod.PRODUCT_IMG_URL)
                                , da.Parameter("_ACTION_FLAG", 1)
                                , da.Parameter("_OUTMSG", String.Empty, true)
                                );

                if (i == 1)
                {
                    return oc[0].strParamValue.ToString();
                }
                else
                    return "Failed";
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return "Failed"; ;
            }

        }
        #endregion
    }
}
